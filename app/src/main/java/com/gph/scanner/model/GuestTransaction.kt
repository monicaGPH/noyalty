package com.gph.scanner.model

/**
 * Created by asus on 16-Aug-18.
 */


data class GuestTransaction(val KEY_GUEST_TRANSACTION_ACTIVITY_ID: String,
                            val KEY_GUEST_TRANSACTION_ID: Int,
                            val KEY_GUEST_TRANSACTION_REDEEM: String,
                            val KEY_GUEST_TRANSACTION_REWARD: Int,
                            val KEY_GUEST_TRANSACTION_STAMP: Int,
                            val KEY_GUEST_TRANSACTION_UPDATEDBY:String,
                            val KEY_GUEST_TRANSACTION_UPDATED: String) {
    constructor(KEY_GUEST_TRANSACTION_ID: Int,
                KEY_GUEST_TRANSACTION_REDEEM: String,
                KEY_GUEST_TRANSACTION_REWARD: Int,
                KEY_GUEST_TRANSACTION_STAMP: Int,
                KEY_GUEST_TRANSACTION_UPDATEDBY:String,
                KEY_GUEST_TRANSACTION_UPDATED: String) :
            this("",
                    KEY_GUEST_TRANSACTION_ID,
                    KEY_GUEST_TRANSACTION_REDEEM,
                    KEY_GUEST_TRANSACTION_REWARD,
                    KEY_GUEST_TRANSACTION_STAMP,
                    KEY_GUEST_TRANSACTION_UPDATEDBY,
                    KEY_GUEST_TRANSACTION_UPDATED)
}