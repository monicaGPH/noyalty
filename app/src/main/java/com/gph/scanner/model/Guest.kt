package com.gph.scanner.model

/**
 * Created by asus on 06-Aug-18.
 */

data class Guest(val guest_id: String,
                 var guest_name: String,
                 val guest_add: String,
                 val guest_mobile_number: String,
                 val guest_email: String,
                 val guest_membershipId: String,
                 val guest_nfc_id: String,
                 val guest_card_id: String,
                 val guest_profile_picture: String,
                 val guest_created_at: String) {

    constructor(guest_created_at: String) :
            this("",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",

                    guest_created_at)

    constructor(guest_name: String,
                guest_add: String,
                guest_mobile_number: String,
                guest_email: String,
                guest_membershipId: String,
                guest_nfc_id: String,
                guest_card_id: String,
                guest_profile_picture: String,
                guest_created_at: String) :
            this("",
                    guest_name,
                    guest_add,
                    guest_mobile_number,
                    guest_email,
                    guest_membershipId,
                    guest_nfc_id,
                    guest_card_id,
                    guest_profile_picture,
                    guest_created_at)

    constructor(guest_name: String,
                guest_add: String,
                guest_mobile_number: String,
                guest_email: String,
                guest_profile_picture: String) :
            this("",
                    guest_name,
                    guest_add,
                    guest_mobile_number,
                    guest_email,
                    "",
                    "",
                    "",
                    guest_profile_picture,
                    "")


}
