package com.gph.scanner.model

/**
 * Created by asus on 03-Aug-18.
 */

data class Account(val emp_fullname: String?,
                       var emp_email: String?,
                       val emp_pwd: String?,
                       val emp_right: String?)

