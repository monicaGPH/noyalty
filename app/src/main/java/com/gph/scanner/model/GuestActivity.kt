package com.gph.scanner.model

/**
 * Created by asus on 13-Aug-18.
 */
data class GuestActivity(val activity_id: String,
                         val activity_store_id: String,
                         val activity_created_at: String,
                         val activity_guest_id: String,
                         val activity_transactionid: String,
                         val lifetime_stamp: String,
                         val credit: String) {
    constructor(activity_store_id: String,
                activity_created_at: String,
                activity_guest_id: String,
                activity_transactionid: String,
                lifetime_stamp: String,
                credit: String) : this("",
            activity_store_id,
            activity_created_at,
            activity_guest_id,
            activity_transactionid,
            lifetime_stamp,
            credit)
}