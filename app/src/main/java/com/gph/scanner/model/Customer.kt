package com.gph.scanner.model

/**
 * Created by asus on 13-Sep-18.
 */
data class Customer(val guest_id: String,
                    var guest_name: String,
                    val guest_add: String,
                    val guest_mobile_number: String,
                    val guest_email: String,
                    val guest_membershipId: String,
                    val guest_nfc_id: String,
                    val guest_card_id: String,
                    val guest_profile_picture: String,
                    val guest_created_at: String,
                    val guest_voucher_value: String,
                    val guest_satmp_value: String)
