package com.gph.scanner.model

/**
 * Created by asus on 04-Oct-18.
 */
data class Voucher(val guest_voucher_id: String,
                   var voucher_value: String,
                   val voucher_created_at: String) {
    constructor(voucher_value: String,
                voucher_created_at: String) :
            this("",
                    voucher_value,
                    voucher_created_at)
}