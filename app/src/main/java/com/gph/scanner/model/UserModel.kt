package com.gph.scanner.model

/**
 * Created by asus on 11-Oct-18.
 */

data class UserModel(val KEY_EMP_ID: String,
                     val KEY_EMP_FULLNMAE: String,
                     var KEY_EMP_EMAIL: String,
                     val KEY_EMP_PWD: String,
                     val KEY_USER_RIGHT: String) {
    constructor(KEY_EMP_FULLNMAE: String,
                KEY_EMP_EMAIL: String,
                KEY_EMP_PWD: String,
                KEY_USER_RIGHT: String) :
            this("",
                    KEY_EMP_FULLNMAE,
                    KEY_EMP_EMAIL,
                    KEY_EMP_PWD,
                    KEY_USER_RIGHT)
}
