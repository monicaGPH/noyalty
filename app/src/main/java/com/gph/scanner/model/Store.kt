package com.gph.scanner.model

/**
 * Created by asus on 10-Aug-18.
 */
data class Store(val store_id: String,
                 val store_name: String,
                 val store_address: String,
                 val store_email_address:String,
                 val membership_id: String) {

    constructor(store_name: String,
                store_address: String,
                store_email_address: String,
                membership_id: String) :
            this("",
                    store_name,
                    store_address,
                    store_email_address,
                    membership_id)
}