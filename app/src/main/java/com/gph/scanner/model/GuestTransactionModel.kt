package com.gph.scanner.model

/**
 * Created by asus on 28-Aug-18.
 */
data class GuestTransactionmodel(val KEY_GUEST_NAME: String,
                                 val KEY_GUEST_ADD: String,
                                 val KEY_GUEST_MOBILE: String,
                                 val KEY_GUEST_EMAIL: String,
                                 val KEY_GUEST_TRANSACTION_ACTIVITY_ID: String,
                                 val KEY_GUEST_TRANSACTION_ID: Int,
                                 val KEY_GUEST_TRANSACTION_REDEEM: String,
                                 val KEY_GUEST_TRANSACTION_REWARD: Int,
                                 val KEY_GUEST_TRANSACTION_STAMP: Int,
                                 val KEY_GUEST_TRANSACTION_UPDATEDBY: String,
                                 val KEY_GUEST_TRANSACTION_UPDATED: String,
                                 val KEY_TOTAL_REDEEM: Int,
                                 val KEY_TOTAL_REWARD: Int,
                                 val KEY_TOTAL_STAMP: Int,
                                 val KEY_VOUCHER:Float)