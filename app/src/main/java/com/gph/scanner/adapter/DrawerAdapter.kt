package com.gph.scanner.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gph.scanner.R
import kotlinx.android.synthetic.main.drawer_list_item.view.*

/**
 * Created by asus on 07-Aug-18.
 */
open class DrawerAdapter(myDataset: Array<String>, listener: OnItemClickListener) : RecyclerView.Adapter<DrawerAdapter.ViewHolder>() {

    private  var mDataset: Array<String> = myDataset
    private  var mListener: OnItemClickListener = listener

    /**
     * Interface for receiving click events from cells.
     */
    interface OnItemClickListener {
        fun onClick(view: View, position: Int)
    }

    /**
     * Custom viewholder for our planet views.
     */
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        // Holds the TextView that will add each animal to
        val mTextView = view.text1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.drawer_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mTextView!!.text = mDataset[position]
        holder.mTextView.setOnClickListener {
            view -> mListener.onClick(view, position) }
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }
}
