package com.gph.scanner.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox

import android.widget.TextView
import com.gph.scanner.R

/**
 * Created by asus on 16-Aug-18.
 */
class ViewAdapter(val userList: ArrayList<String>) : RecyclerView.Adapter<ViewAdapter.ViewHolder>() {

    companion object {

        var countRedeem: Int = 0
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    fun sizes(): Int {

        var size = itemCount
        return size
    }

    fun removeItemAtPosition(position: Int) {
        userList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, userList.size)
    }

    //the class is hodling the list view
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindItems(user: String) {
            val textViewName = view.findViewById<TextView>(R.id.txt_item)
            val checkBox = view.findViewById<CheckBox>(R.id.checkBox)
            textViewName.text = user

            checkBox?.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    ++countRedeem
                } else {
                    --countRedeem
                }
            }
        }

    }
}