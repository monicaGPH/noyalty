package com.gph.scanner.adapter

import android.app.AlertDialog
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.model.UserModel
import com.gph.scanner.utils.database.DatabaseHandler
import java.util.*

/**
 * Created by asus on 11-Oct-18.
 */
class UserAdapter(var userList: ArrayList<UserModel>, val contexts: Context, val databaseHandler: DatabaseHandler, val existingLocale: Locale, val logged_in_username: String) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_user_list, parent, false)
        return ViewHolder(v, contexts)
    }

    //this method is binding the data on the list

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        val users = userList[position]
        var isEditButtonCall: Boolean = false

        holder.et_user_value.setText(users.KEY_EMP_FULLNMAE)
        holder.et_email_address_value.setText(users.KEY_EMP_EMAIL)
        holder.et_role_value.setText(users.KEY_USER_RIGHT)

        holder.et_user_value.isEnabled = false
        holder.et_role_value.isEnabled = false
        holder.et_email_address_value.isEnabled = false

        holder.txt_edit_details.setOnClickListener {

            if (!isEditButtonCall) {
                isEditButtonCall = true
                holder.txt_edit_details.text = "Ok"
                holder.et_user_value.isEnabled = true
                holder.et_email_address_value.isEnabled = true


                holder.et_user_value.setBackgroundColor(contexts.resources.getColor(R.color.darkWhite))
                holder.et_email_address_value.setBackgroundColor(contexts.resources.getColor(R.color.darkWhite))

            } else {
                holder.txt_edit_details.text = contexts.getString(R.string.edit_card_details_text_string)
                holder.et_user_value.isEnabled = false
                holder.et_email_address_value.isEnabled = false


                databaseHandler.updateEmployee(
                        UserModel(
                                holder.et_user_value.text.toString().trim(),
                                holder.et_email_address_value.text.toString().trim(),
                                users.KEY_EMP_PWD,
                                holder.et_role_value.text.toString().trim()
                        ), users.KEY_EMP_ID.toInt())
                isEditButtonCall = false
            }

        }
        holder.txt_delete_details.setOnClickListener {

            if (logged_in_username == users.KEY_EMP_EMAIL ||
                    logged_in_username == users.KEY_EMP_FULLNMAE) {
                Toast.makeText(contexts, contexts.getString(R.string.user_self_alert_text_string), Toast.LENGTH_SHORT).show()
            } else {
                // Initialize a new instance of
                val builder = AlertDialog.Builder(contexts)

                // Set the alert dialog title
                builder.setTitle(contexts.getString(R.string.app_name))

                // Display a message on alert dialog
                builder.setMessage(contexts.getString(R.string.delete_user_alert_text_string))

                // Set a positive button and its click listener on alert dialog
                builder.setPositiveButton("OK") { dialog, _ ->

                    dialog.dismiss()
                    databaseHandler.deleteEmployee(users.KEY_EMP_ID)
                    removeItemAtPosition(position)
                    Toast.makeText(contexts, contexts.getString(R.string.delete_card_text_string), Toast.LENGTH_SHORT).show()


                }

                // Set a negative button and its click listener on alert dialog
                builder.setNegativeButton(if (existingLocale.toLanguageTag() == "en") "Cancel" else "Abbrechen") { dialog, _ ->
                    dialog.dismiss()


                }
                // Finally, make the alert dialog using builder
                val dialog: AlertDialog = builder.create()
                // Display the alert dialog on app interface
                dialog.show()

            }

        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size

    }

    fun removeItemAtPosition(position: Int) {
        userList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, userList.size)
    }

    fun filterList(filterdNames: ArrayList<UserModel>) {
        userList = filterdNames
        notifyDataSetChanged()
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View, contexts: Context) : RecyclerView.ViewHolder(itemView) {

        val et_user_value = itemView.findViewById<EditText>(R.id.et_user_value)
        val et_email_address_value = itemView.findViewById<EditText>(R.id.et_email_address_value)
        val et_role_value = itemView.findViewById<EditText>(R.id.et_role_value)
        val txt_delete_details = itemView.findViewById<TextView>(R.id.txt_delete_details)
        val txt_edit_details = itemView.findViewById<TextView>(R.id.txt_edit_details)


    }
}