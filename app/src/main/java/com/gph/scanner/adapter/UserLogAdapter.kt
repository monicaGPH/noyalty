package com.gph.scanner.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gph.scanner.R
import com.gph.scanner.model.GuestTransactionmodel
import android.text.style.UnderlineSpan
import android.text.SpannableString
import com.gph.scanner.activity.ViewCustomerInfomationActivity
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by asus on 28-Aug-18.
 */
class UserLogAdapter(var userList: ArrayList<GuestTransactionmodel>, val contexts: Context, private val existingLocale: Locale) : RecyclerView.Adapter<UserLogAdapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserLogAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_user_activity_log, parent, false)
        return ViewHolder(v, contexts, existingLocale)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: UserLogAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position], contexts, existingLocale)

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size

    }

    fun filterList(filterdNames: ArrayList<GuestTransactionmodel>) {
        userList = filterdNames
        notifyDataSetChanged()
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View, contexts: Context, existingLocale: Locale) : RecyclerView.ViewHolder(itemView) {
        private lateinit var content: SpannableString
        private lateinit var txt: String
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun bindItems(guestTransactionmodel: GuestTransactionmodel, contexts: Context, existingLocale: Locale) {
            val txt_user_value = itemView.findViewById<TextView>(R.id.txt_user_value)
            val txt_customer_value = itemView.findViewById<TextView>(R.id.txt_customer_value)
            val txt_event_value = itemView.findViewById<TextView>(R.id.txt_event_value)
            val txt_dateandtime_value = itemView.findViewById<TextView>(R.id.txt_dateandtime_value)
            txt_user_value.text = guestTransactionmodel.KEY_GUEST_TRANSACTION_UPDATEDBY

            if (existingLocale.toLanguageTag() == "en") {
                txt_customer_value.text = guestTransactionmodel.KEY_GUEST_NAME

                content = if(guestTransactionmodel.KEY_GUEST_NAME.isNullOrEmpty()){
                    SpannableString(contexts.getString(R.string.customer_name_not_set_text_string))
                } else{
                    SpannableString(guestTransactionmodel.KEY_GUEST_NAME)
                }
            } else if (existingLocale.toLanguageTag() == "de") {
                txt_customer_value.text = guestTransactionmodel.KEY_GUEST_NAME
                content = if(guestTransactionmodel.KEY_GUEST_NAME.isNullOrEmpty()){
                    SpannableString(contexts.getString(R.string.customer_name_not_set_text_string))
                } else{
                    SpannableString(guestTransactionmodel.KEY_GUEST_NAME)
                }
            }
            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            txt_customer_value.text = content

            var dateandtimeArray = parseDateToddMMMyyyy(guestTransactionmodel.KEY_GUEST_TRANSACTION_UPDATED)!!.split(" ")

            if (dateandtimeArray.isNotEmpty()) {
                if (existingLocale.toLanguageTag() == "en")
                    txt_dateandtime_value.text = dateandtimeArray[0] + ", at " + dateandtimeArray[1]
                else
                    txt_dateandtime_value.text = dateandtimeArray[0] + ", um " + dateandtimeArray[1]
            }

            when {
                guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM.toInt() > 0 -> {
                    if (existingLocale.toLanguageTag() == "en")
                        txt = if (guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM > "1") guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM + " bonus redeemed" else guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM + " bonus redeemed"
                    else
                        txt = if (guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM > "1") guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM + " Prämie eingelöst" else guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM + " Prämie eingelöst"
                    txt_event_value.text = txt
                }
                guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD > 0 -> {
                    if (existingLocale.toLanguageTag() == "en")
                        txt = if (guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD > 1) guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD.toString() + " bonus earned" else guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD.toString() + " bonus earned"
                    else
                        txt = if (guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD > 1) guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD.toString() + " Prämien erhalten" else guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD.toString() + " Prämien erhalten"
                    txt_event_value.text = txt
                }
                guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP > 0 -> {
                    if (existingLocale.toLanguageTag() == "en")
                        txt = if (guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP > 1) guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP.toString() + " Stamps received" else guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP.toString() + " Stamp received"
                    else
                        txt = if (guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP > 1) guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP.toString() + " Stempel erhalten" else guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP.toString() + " Stempel verdient"
                    txt_event_value.text = txt
                }
            }
            txt_customer_value.setOnClickListener {
                val intent = Intent(contexts, ViewCustomerInfomationActivity::class.java)
                intent.putExtra("KEY_NAME", guestTransactionmodel.KEY_GUEST_NAME)
                intent.putExtra("KEY_ADD", guestTransactionmodel.KEY_GUEST_ADD)
                intent.putExtra("KEY_MOBILE", guestTransactionmodel.KEY_GUEST_MOBILE)
                intent.putExtra("KEY_EMAIL", guestTransactionmodel.KEY_GUEST_EMAIL)
                intent.putExtra("KEY_REDEEM", guestTransactionmodel.KEY_TOTAL_REDEEM)
                intent.putExtra("KEY_REWARD", guestTransactionmodel.KEY_TOTAL_REWARD)
                intent.putExtra("KEY_STAMP", guestTransactionmodel.KEY_TOTAL_STAMP)
                intent.putExtra("KEY_VOUCHER", guestTransactionmodel.KEY_VOUCHER)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                contexts.startActivity(intent)

            }

        }


        fun parseDateToddMMMyyyy(time: String): String? {

            //2017-09-16 02:28:21
            val inputPattern = "dd/M/yyyy  HH:mm"
            val outputPattern = "yyyy-M-dd HH:mm"

            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            //   outputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
            var date: Date?
            var str: String? = null


            try {
                date = inputFormat.parse(time)
                str = outputFormat.format(date)


            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str

        }
    }


}