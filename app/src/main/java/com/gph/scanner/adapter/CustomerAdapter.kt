package com.gph.scanner.adapter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.gph.scanner.R

import com.gph.scanner.activity.DashBoardActivity
import com.gph.scanner.activity.ViewCustomerActivity
import com.gph.scanner.model.*
import com.gph.scanner.utils.RoundedImageView
import com.gph.scanner.utils.database.DatabaseHandler
import com.squareup.picasso.Picasso

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by asus on 13-Sep-18.
 */
class CustomerAdapter(var customerList: ArrayList<Guest_Info>,
                      val contexts: Context,
                      val isCallFromActivity: Boolean,
                      val scanValue: String,
                      var scanType: String,
                      var language: Locale,
                      val guest: Guest?,
                      val guestActivity: GuestActivity?,
                      val voucher: Voucher?,
                      val role: String,
                      val activity: ViewCustomerActivity,
                      var username: String) : RecyclerView.Adapter<CustomerAdapter.ViewHolder>() {
    var databaseHandler: DatabaseHandler = DatabaseHandler(contexts)

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_customer_info, parent, false)

        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: CustomerAdapter.ViewHolder, position: Int) {
        // val check = holder.bindItems(customerList[position], contexts, isCallFromActivity, scanValue, scanType, databaseHandler, position, language)
        val customer = customerList[position]
        var guest_ids = 0
        var guest_membership_ids = ""
        var isEditButtonCall = false

        holder.txt_customer_name_value.setText(if (customer.guest_name.isNullOrEmpty()) contexts.getString(R.string.customer_name_not_set_text_string) else customer.guest_name)
        holder.txt_customer_email_address_value.setText(if (customer.guest_email.isNullOrEmpty()) contexts.getString(R.string.customer_email_address_not_set_text_string) else customer.guest_email)
        holder.txt_customer_mobile_number_value.setText(if (customer.guest_mobile_number.isNullOrEmpty()) contexts.getString(R.string.customer_mobile_number_not_set_text_string) else customer.guest_mobile_number)
        holder.txt_customer_address_value.setText(if (customer.guest_add.isNullOrEmpty()) contexts.getString(R.string.customer_address_not_set_text_string) else customer.guest_add)
        holder.txt_customer_voucher_value.setText(customer.guest_voucher_value + " " + contexts.getString(R.string.euro_symbol))
        holder.txt_customer_reward_value.setText(customer.reward_value)
        holder.txt_customer_stamp_value.setText(customer.guest_satmp_value)

        if (role == "admin") {
            holder.txt_edit_info.visibility = View.VISIBLE
            holder.txt_delete_card.visibility = View.VISIBLE
        } else {
            holder.txt_edit_info.visibility = View.GONE
            holder.txt_delete_card.visibility = View.GONE
        }

        if (!customer.guest_nfc_id.isEmpty())
            holder.txt_customer_cardid_value.text = if (customer.guest_nfc_id.isNullOrEmpty()) "Card Value not exist" else customer.guest_nfc_id
        else
            holder.txt_customer_cardid_value.text = if (customer.guest_card_id.isNullOrEmpty()) contexts.getString(R.string.card_value_text_string) else customer.guest_card_id

        holder.txt_customer_name_value.isEnabled = false
        holder.txt_customer_email_address_value.isEnabled = false
        holder.txt_customer_mobile_number_value.isEnabled = false
        holder.txt_customer_address_value.isEnabled = false
        holder.txt_customer_voucher_value.isEnabled = false
        holder.txt_customer_stamp_value.isEnabled = false
        holder.txt_customer_cardid_value.isEnabled = false
        holder.txt_customer_reward_value.isEnabled = false

        Picasso.get()
                .load("file://" + customer.guest_profile_picture)
                .placeholder(R.drawable.ic_person) //this is optional the image to display while the url image is downloading//this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(holder.imageView_rounded)

        val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
        val currentDate = sdf.format(Date())

        holder.txt_edit_info.setOnClickListener {
            if (!isEditButtonCall) {
                isEditButtonCall = true
                holder.txt_edit_info.text = "Ok"
                holder.txt_customer_name_value.isEnabled = true
                holder.txt_customer_email_address_value.isEnabled = true
                holder.txt_customer_mobile_number_value.isEnabled = true
                holder.txt_customer_address_value.isEnabled = true

                holder.txt_customer_name_value.requestFocus()

                holder.txt_customer_email_address_value.setText(if (holder.txt_customer_email_address_value.text.toString() == contexts.getString(R.string.customer_email_address_not_set_text_string)) "" else customer.guest_email)
                holder.txt_customer_name_value.setText(if (holder.txt_customer_name_value.text.toString() == contexts.getString(R.string.customer_name_not_set_text_string)) "" else customer.guest_name)
                holder.txt_customer_mobile_number_value.setText(if (holder.txt_customer_mobile_number_value.text.toString() == contexts.getString(R.string.customer_mobile_number_not_set_text_string)) "" else customer.guest_mobile_number)
                holder.txt_customer_address_value.setText(if (holder.txt_customer_address_value.text.toString() == contexts.getString(R.string.customer_address_not_set_text_string)) "" else customer.guest_add)

                holder.txt_customer_name_value.setBackgroundColor(contexts.resources.getColor(R.color.darkWhite))
                holder.txt_customer_email_address_value.setBackgroundColor(contexts.resources.getColor(R.color.darkWhite))
                holder.txt_customer_mobile_number_value.setBackgroundColor(contexts.resources.getColor(R.color.darkWhite))
                holder.txt_customer_address_value.setBackgroundColor(contexts.resources.getColor(R.color.darkWhite))

            } else {
                holder.txt_edit_info.text = contexts.getString(R.string.edit_card_details_text_string)
                holder.txt_customer_name_value.isEnabled = false
                holder.txt_customer_email_address_value.isEnabled = false
                holder.txt_customer_mobile_number_value.isEnabled = false
                holder.txt_customer_address_value.isEnabled = false


                if (customer.guest_card_id.isNullOrEmpty()) {
                    databaseHandler.updateGuest(Guest(
                            if (holder.txt_customer_name_value.text.toString() == contexts.getString(R.string.customer_name_not_set_text_string)) "" else holder.txt_customer_name_value.text.toString().trim(),
                            if (holder.txt_customer_address_value.text.toString() == contexts.getString(R.string.customer_address_not_set_text_string)) "" else holder.txt_customer_address_value.text.toString().trim(),
                            if (holder.txt_customer_mobile_number_value.text.toString() == contexts.getString(R.string.customer_mobile_number_not_set_text_string)) "" else holder.txt_customer_mobile_number_value.text.toString().trim(),
                            if (holder.txt_customer_email_address_value.text.toString() == contexts.getString(R.string.customer_email_address_not_set_text_string)) "" else holder.txt_customer_email_address_value.text.toString().trim(),
                            "",
                            customer.guest_nfc_id,
                            "",
                            customer.guest_profile_picture,
                            currentDate),
                            customer.guest_nfc_id,
                            "NFC")
                } else {
                    databaseHandler.updateGuest(Guest(
                            if (holder.txt_customer_name_value.text.toString() == contexts.getString(R.string.customer_name_not_set_text_string)) "" else holder.txt_customer_name_value.text.toString().trim(),
                            if (holder.txt_customer_address_value.text.toString() == contexts.getString(R.string.customer_address_not_set_text_string)) "" else holder.txt_customer_address_value.text.toString().trim(),
                            if (holder.txt_customer_mobile_number_value.text.toString() == contexts.getString(R.string.customer_mobile_number_not_set_text_string)) "" else holder.txt_customer_mobile_number_value.text.toString().trim(),
                            if (holder.txt_customer_email_address_value.text.toString() == contexts.getString(R.string.customer_email_address_not_set_text_string)) "" else holder.txt_customer_email_address_value.text.toString().trim(),
                            "",
                            "",
                            customer.guest_card_id,
                            customer.guest_profile_picture,
                            currentDate),
                            customer.guest_card_id,
                            "CARD")
                }
                isEditButtonCall = false
            }
        }

        holder.txt_delete_card.setOnClickListener {
            if (!scanValue.isNullOrEmpty()) {
                if (customer.guest_card_id == scanValue ||
                        customer.guest_nfc_id == scanValue) {
                    Toast.makeText(contexts, contexts.getString(R.string.customer_sellf_alert_text_string), Toast.LENGTH_SHORT).show()
                } else {
                    // Initialize a new instance of
                    val builder = AlertDialog.Builder(contexts)

                    // Set the alert dialog title
                    builder.setTitle(contexts.getString(R.string.app_name))

                    // Display a message on alert dialog
                    builder.setMessage(contexts.getString(R.string.delete_card_alert_text_string))

                    // Set a positive button and its click listener on alert dialog
                    builder.setPositiveButton("OK") { dialog, _ ->

                        dialog.dismiss()

                        if (!customer.guest_card_id.isNullOrEmpty()) {
                            databaseHandler.deleteGuestActivityData(customer.guest_id)
                            databaseHandler.deleteGuestTransactionActivityData(customer.guest_id)
                            databaseHandler.deleteGuestQRCodeData(customer.guest_card_id)
                        } else {
                            databaseHandler.deleteGuestActivityData(customer.guest_id)
                            databaseHandler.deleteGuestTransactionActivityData(customer.guest_id)
                            databaseHandler.deleteGuestNFCData(customer.guest_nfc_id)
                        }
                        removeItemAtPosition(position)
                        Toast.makeText(contexts, contexts.getString(R.string.delete_card_text_string), Toast.LENGTH_SHORT).show()


                    }

                    // Set a negative button and its click listener on alert dialog
                    builder.setNegativeButton(if (language.toLanguageTag() == "en") "Cancel" else "Abbrechen") { dialog, _ ->
                        dialog.dismiss()


                    }
                    // Finally, make the alert dialog using builder
                    val dialog: AlertDialog = builder.create()
                    // Display the alert dialog on app interface
                    dialog.show()
                }
            } else {
                // Initialize a new instance of
                val builder = AlertDialog.Builder(contexts)

                // Set the alert dialog title
                builder.setTitle(contexts.getString(R.string.app_name))

                // Display a message on alert dialog
                builder.setMessage(contexts.getString(R.string.delete_card_alert_text_string))

                // Set a positive button and its click listener on alert dialog
                builder.setPositiveButton("OK") { dialog, _ ->

                    dialog.dismiss()

                    if (!customer.guest_card_id.isNullOrEmpty())
                        databaseHandler.deleteGuestQRCodeData(customer.guest_card_id)
                    else
                        databaseHandler.deleteGuestNFCData(customer.guest_nfc_id)
                    removeItemAtPosition(position)
                    Toast.makeText(contexts, contexts.getString(R.string.delete_card_text_string), Toast.LENGTH_SHORT).show()


                }

                // Set a negative button and its click listener on alert dialog
                builder.setNegativeButton(if (language.toLanguageTag() == "en") "Cancel" else "Abbrechen") { dialog, _ ->
                    dialog.dismiss()


                }
                // Finally, make the alert dialog using builder
                val dialog: AlertDialog = builder.create()
                // Display the alert dialog on app interface
                dialog.show()
            }
        }
        if (isCallFromActivity) {

            holder.root_layout.setOnClickListener {
                if (scanType.equals("NFC", true)) {
                    var (guest_id, guest_membership_id, guest_card_id) = databaseHandler.getGuestIDBYnfcId(scanValue)!!
                    guest_ids = guest_id
                    guest_membership_ids = guest_membership_id

                } else if (scanType.equals("CARD", true)) {
                    val (guest_id, guest_membership_id, guest_card_id) = databaseHandler.getGuestIDBYCARDId(scanValue)!!
                    guest_ids = guest_id
                    guest_membership_ids = guest_membership_id
                }
                if (guest_membership_ids.isNullOrEmpty()){
                // NFC type
                    if (scanType == "NFC") {
                        databaseHandler.updateGuest(Guest(
                                if (customer.guest_name == contexts.getString(R.string.customer_name_not_set_text_string)) "" else customer.guest_name,
                                if (customer.guest_add == contexts.getString(R.string.customer_address_not_set_text_string)) "" else customer.guest_add,
                                if (customer.guest_mobile_number == contexts.getString(R.string.customer_mobile_number_not_set_text_string)) "" else customer.guest_mobile_number,
                                if (customer.guest_email == contexts.getString(R.string.customer_email_address_not_set_text_string)) "" else customer.guest_email,
                                "linked",
                                scanValue,
                                "",
                                customer.guest_profile_picture,
                                currentDate), scanValue, scanType)

                        var stampValue: String = ""
                        var rewardValue = 0
                        if (guestActivity != null) {
                            if (!guestActivity.lifetime_stamp.isNullOrEmpty()) {
                                val stamptempValue = guestActivity.lifetime_stamp.toInt() + customer.guest_satmp_value.toInt()
                                if (stamptempValue >= 5) {
                                    rewardValue = 1
                                    stampValue = "0"
                                } else {
                                    stampValue = stamptempValue.toString()
                                }

                            }
                        } else {
                            stampValue = customer.guest_satmp_value
                        }
                        if (guestActivity != null) {
                            if (!guestActivity.credit.isNullOrEmpty()) {
                                val rewardtempValue = guestActivity.credit.toInt() + customer.reward_value.toInt()
                                rewardValue = rewardtempValue + rewardValue
                            }
                        } else {
                            rewardValue = customer.reward_value.toInt()
                        }
                        // check guestActivity exist or not
                        if (!databaseHandler.checkGuestActivityExistorNot(guest_ids)) {
                            databaseHandler.addGuestActivity(GuestActivity(
                                    "",
                                    currentDate,
                                    guest_ids.toString(),
                                    "0",
                                    stampValue,
                                    rewardValue.toString()))
                        } else {
                            databaseHandler.updateGuestActivity(
                                    GuestActivity(
                                            "",
                                            currentDate,
                                            "",
                                            "0",
                                            stampValue,
                                            rewardValue.toString()),
                                    guest_ids.toString())
                        }

                        databaseHandler.addGuestTransaction(
                                GuestTransaction(
                                        guest_ids.toInt(),
                                        "0",
                                        rewardValue,
                                        stampValue.toInt(),
                                        username,
                                        currentDate))
                        var voucher_value: String? = null
                        voucher_value = if (voucher != null) {
                            if (!voucher!!.voucher_value.isNullOrEmpty()) (voucher.voucher_value.toInt() + customer.guest_voucher_value.toInt()).toString() else customer.guest_voucher_value
                        } else {
                            customer.guest_voucher_value
                        }
                        if (!databaseHandler.checkVoucherInformationExistorNot(guest_ids.toString())) {
                            databaseHandler.addVoucher(Voucher(
                                    guest_ids.toString(),
                                    if (voucher_value.isNullOrEmpty()) "" else voucher_value!!,
                                    currentDate))
                        } else {
                            databaseHandler.updateVoucher(Voucher(
                                    if (voucher_value.isNullOrEmpty()) "" else voucher_value!!,
                                    currentDate),
                                    guest_ids.toString())
                        }
                        databaseHandler.deleteGuestActivityData(customer.guest_id)
                        databaseHandler.deleteGuestTransactionActivityData(customer.guest_id)
                        databaseHandler.deleteGuestNFCData(customer.guest_nfc_id)
                        Toast.makeText(contexts, contexts.getString(R.string.customer_link_toast_text_string), Toast.LENGTH_SHORT).show()
                    }
                    // card type
                    else {
                        /*if (scanType.equals("NFC", true)) {
                        var (guest_id, guest_card_id) = databaseHandler.getGuestIDBYnfcId(scanValue)!!
                        guest_ids = guest_id
                    } else if (scanType.equals("CARD", true)) {
                        val (guest_id, guest_card_id) = databaseHandler.getGuestIDBYCARDId(scanValue)!!
                        guest_ids = guest_id
                    }*/
                        databaseHandler.updateGuest(Guest(
                                if (customer.guest_name == contexts.getString(R.string.customer_name_not_set_text_string)) "" else customer.guest_name,
                                if (customer.guest_add == contexts.getString(R.string.customer_address_not_set_text_string)) "" else customer.guest_add,
                                if (customer.guest_mobile_number == contexts.getString(R.string.customer_mobile_number_not_set_text_string)) "" else customer.guest_mobile_number,
                                if (customer.guest_email == contexts.getString(R.string.customer_email_address_not_set_text_string)) "" else customer.guest_email,
                                "linked",
                                "",
                                scanValue,
                                customer.guest_profile_picture,
                                currentDate), scanValue, scanType)
                        var stampValue: String = ""
                        var rewardValue = 0
                        if (guestActivity != null) {
                            if (!guestActivity.lifetime_stamp.isNullOrEmpty()) {
                                val stamptempValue = guestActivity.lifetime_stamp.toInt() + customer.guest_satmp_value.toInt()
                                if (stamptempValue >= 5) {
                                    rewardValue = 1
                                    stampValue = "0"
                                } else {
                                    stampValue = stamptempValue.toString()
                                }

                            }
                        } else {
                            stampValue = customer.guest_satmp_value
                        }
                        if (guestActivity != null) {
                            if (!guestActivity.credit.isNullOrEmpty()) {
                                val rewardtempValue = guestActivity.credit.toInt() + customer.reward_value.toInt()
                                rewardValue += rewardtempValue
                            }
                        } else {
                            rewardValue = customer.reward_value.toInt()
                        }
                        // check guestActivity exist or not
                        if (!databaseHandler.checkGuestActivityExistorNot(guest_ids)) {
                            databaseHandler.addGuestActivity(GuestActivity(
                                    "",
                                    currentDate,
                                    guest_ids.toString(),
                                    "0",
                                    stampValue,
                                    rewardValue.toString()))

                        } else {
                            databaseHandler.updateGuestActivity(
                                    GuestActivity(
                                            "",
                                            currentDate,
                                            "",
                                            "0",
                                            stampValue!!,
                                            rewardValue.toString()),
                                    guest_ids.toString())
                        }
                        databaseHandler.addGuestTransaction(
                                GuestTransaction(
                                        guest_ids.toInt(),
                                        "0",
                                        rewardValue,
                                        stampValue.toInt(),
                                        username,
                                        currentDate))
                        var voucher_value: String? = null
                        voucher_value = if (voucher != null) {
                            if (!voucher.voucher_value.isNullOrEmpty()) (voucher.voucher_value.toFloat() + customer.guest_voucher_value.toFloat()).toString() else customer.guest_voucher_value
                        } else {
                            customer.guest_voucher_value
                        }
                        if (!databaseHandler.checkVoucherInformationExistorNot(guest_ids.toString())) {
                            databaseHandler.addVoucher(Voucher(
                                    guest_ids.toString(),
                                    if (voucher_value.isNullOrEmpty()) "" else voucher_value!!,
                                    currentDate))
                        } else {
                            databaseHandler.updateVoucher(Voucher(
                                    if (voucher_value.isNullOrEmpty()) "" else voucher_value!!,
                                    currentDate),
                                    guest_ids.toString())
                        }

                        databaseHandler.deleteGuestActivityData(customer.guest_id)
                        databaseHandler.deleteGuestTransactionActivityData(customer.guest_id)
                        databaseHandler.deleteGuestQRCodeData(customer.guest_card_id)
                        Toast.makeText(contexts, contexts.getString(R.string.customer_link_toast_text_string), Toast.LENGTH_SHORT).show()

                    }


                val intent = Intent(contexts, DashBoardActivity::class.java)

                if (scanType == "NFC") {
                    intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                } else {
                    intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                }
                intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                contexts.startActivity(intent)
                (contexts as Activity).finish()
                } else {
                    activity.showMessageOK(contexts.getString(R.string.link_error_alert_text_string))
                }
            }

        }

    }


    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return customerList.size

    }

    fun filterList(filterdNames: ArrayList<Guest_Info>) {
        customerList = filterdNames
        notifyDataSetChanged()
    }


    fun removeItemAtPosition(position: Int) {
        customerList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, customerList.size)
    }


    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val txt_customer_name_value = itemView.findViewById<EditText>(R.id.txt_customer_name_value)
        val txt_customer_email_address_value = itemView.findViewById<EditText>(R.id.txt_customer_email_address_value)
        val txt_customer_mobile_number_value = itemView.findViewById<EditText>(R.id.txt_customer_mobile_number_value)
        val txt_customer_address_value = itemView.findViewById<EditText>(R.id.txt_customer_address_value)
        val txt_customer_cardid_value = itemView.findViewById<TextView>(R.id.txt_card_id_value)
        val txt_customer_voucher_value = itemView.findViewById<EditText>(R.id.txt_customer_voucher_value)
        val txt_customer_stamp_value = itemView.findViewById<EditText>(R.id.txt_customer_stamp_value)
        val txt_customer_reward_value = itemView.findViewById<EditText>(R.id.txt_customer_reward_value)
        val txt_edit_info = itemView.findViewById<TextView>(R.id.txt_edit_info)
        val txt_delete_card = itemView.findViewById<TextView>(R.id.txt_delete_card)
        val root_layout = itemView.findViewById<RelativeLayout>(R.id.root_layout)
        val imageView_rounded = itemView.findViewById<RoundedImageView>(R.id.imageView_rounded)


    }


}



