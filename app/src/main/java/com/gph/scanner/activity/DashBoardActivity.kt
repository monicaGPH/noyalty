package com.gph.scanner.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.telephony.SmsManager
import android.view.ContextMenu
import android.view.MenuItem
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.fragment.dialog.PasswordDialog

import com.gph.scanner.model.Guest
import com.gph.scanner.model.Store
import com.gph.scanner.model.Voucher
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.GMailSender
import com.gph.scanner.utils.database.DatabaseHandler
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.text.SimpleDateFormat
import java.util.*

import android.view.View
import android.view.Window
import android.widget.TextView
import me.dm7.barcodescanner.zbar.ZBarScannerView


/**
 * Created by asus on 13-Sep-18.
 */
class DashBoardActivity : SActivity() {
    private lateinit var guest_card_id: String
    private lateinit var guest_card_type: String
    lateinit var guest: Guest
    lateinit var role: String
    lateinit var existingLocale: Locale
    lateinit var voucher: Voucher
    var store: Store? = null
    var redeems: Int = 0
    var PERMISSION_REQUEST_CODE: Int = 333
    var voucher_value: Float = 0F
    var rewards: Int = 0
    var stamps: Int = 0
    private lateinit var databaseHandler: DatabaseHandler


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // set layout
        setContentView(R.layout.activity_dashboard)
        // set custom actionBar Name
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.dashboard_text_string)
        // create DB instance
        databaseHandler = DatabaseHandler(applicationContext)
        // get current language
        existingLocale = currentLanguage
        //get role of loggedInUser
        role = getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERRIGHT, "")

        if (intent != null) {
            guest_card_id = intent.getStringExtra("INTENT_EXTRA_SCAN_ID")
            guest_card_type = intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE")

        }


        guest = if (guest_card_type == "NFC") {
            databaseHandler.getGuestBYnfcId(guest_card_id)!!
        } else {
            databaseHandler.getGuestBYcardId(guest_card_id)!!
        }
        // set scanned customer name
        txt_customer_name_value.text = if (!guest.guest_name.isNullOrEmpty()) guest.guest_name else getString(R.string.customer_name_not_set_text_string)

        fl_issue_voucher.setOnClickListener {
            if (role == "admin") {
                val intent = Intent(this, IssueActivity::class.java)

                intent.putExtra("INTENT_EXTRA_GUEST_CARD_ID", guest_card_id)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
                intent.putExtra("INTENT_EXTRA_CALL", "Issue")
                startActivityForResult(intent, 1)

            } else {
                Toast.makeText(applicationContext, getString(R.string.issue_voucher_permission_text_string), Toast.LENGTH_SHORT).show()
            }
        }

        fl_burn_voucher.setOnClickListener {

            val intent = Intent(this, IssueActivity::class.java)

            intent.putExtra("INTENT_EXTRA_GUEST_CARD_ID", guest_card_id)
            intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
            intent.putExtra("INTENT_EXTRA_CALL", "Burn")

            startActivityForResult(intent, 1)

        }
        fl_punchcard.setOnClickListener {
            val intent = Intent(this, ShowCustomerInfoActivity::class.java)
            intent.putExtra("INTENT_EXTRA_GUEST_CARD_ID", guest_card_id)
            intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
            startActivity(intent)

        }

        fl_send_info_to_customer.setOnClickListener {
            openDialogBox(it)
        }

        btn_delete_card.setOnClickListener {
            showMessageOK(getString(R.string.delete_card_alert_text_string), false, "")
        }
        btn_add_new_customer.setOnClickListener {
            val intent = Intent(this, AddNewCustomerActivity::class.java)
            intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
            intent.putExtra("INTENT_EXTRA_SCAN_ID", guest_card_id)
            intent.putExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY", "DashboardClass")
            startActivity(intent)
            finish()
        }

        btn_link_to_existing_customer.setOnClickListener {
            val intent = Intent(applicationContext, ViewCustomerActivity::class.java)
            intent.putExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY", "ADDCUSTOMER")
            intent.putExtra("INTENT_EXTRA_CARD_TYPE", guest_card_type)
            intent.putExtra("INTENT_EXTRA_CARD_VALUE", guest_card_id)
            intent.putExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY_SECONDARY", "DashboardClass")
            startActivity(intent)
            finish()
        }
        btn_view_customer.setOnClickListener {
            if (databaseHandler.checkGuestTransactionInformationExistorNot(guest.guest_id)) {
                val guestTransactionmodel = databaseHandler.getGuestTransactionBYGuestID(guest.guest_id.toInt())
                Logger.d(guestTransactionmodel)
                if (guestTransactionmodel != null) {
                    redeems = guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM.toInt()
                    rewards = guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD
                    stamps = guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP
                }
            }
            if (databaseHandler.checkVoucherInformationExistorNot(guest.guest_id)) {
                voucher = databaseHandler.getVoucherInformation(guest.guest_id)!!
                voucher_value = voucher.voucher_value.toFloat()
            }

            val intent = Intent(this, ViewCustomerInfomationActivity::class.java)
            intent.putExtra("INTENT_EXTRA_CARD_TYPE", guest_card_type)
            intent.putExtra("INTENT_EXTRA_CARD_VALUE", guest_card_id)
            intent.putExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY_SECONDARY", "DashboardClass")
            intent.putExtra("INTENT_EXTRA_GUEST_NAME", guest.guest_name)
            intent.putExtra("INTENT_EXTRA_GUEST_ADDRESS", guest.guest_add)
            intent.putExtra("INTENT_EXTRA_GUEST_MOBILE", guest.guest_mobile_number)
            intent.putExtra("INTENT_EXTRA_GUEST_EMAIL", guest.guest_email)
            intent.putExtra("INTENT_EXTRA_GUEST_STAMP_VALUE", if (stamps == 0) 0 else stamps)
            intent.putExtra("INTENT_EXTRA_GUEST_REWARD_REDEEM", if (redeems == 0) 0 else redeems)
            intent.putExtra("INTENT_EXTRA_GUEST_REWARD_AWAILABLE", if (rewards == 0) 0 else rewards)
            intent.putExtra("INTENT_EXTRA_GUEST_VOUCHER", if (voucher_value == 0F) 0F else voucher_value)
            startActivity(intent)
            finish()
        }

    }


    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@DashBoardActivity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            guest_card_type = data.getStringExtra("ScanType")
            guest_card_id = data.getStringExtra("ScanValue")
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showMessageOK(message: String, isNormalAlert: Boolean, task: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@DashBoardActivity)

        // Set the alert dialog title
        builder.setTitle(resources.getString(R.string.app_name))

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            if (task == "sendEmail") {
                SendEmailTask(redeems,
                        rewards,
                        stamps,
                        voucher_value,
                        applicationContext,
                        store!!.store_email_address,
                        guest.guest_email,
                        getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_GMAIL_PASSWORD, "")).execute()

            }
            dialog.dismiss()
            if (!isNormalAlert) {
                databaseHandler.deleteGuestCardId(guest_card_id, guest_card_type)
                Toast.makeText(applicationContext, getString(R.string.delete_card_text_string), Toast.LENGTH_SHORT).show()
                val intent = Intent(this, DrawerActivity::class.java)
                startActivity(intent)
                this.finish()
            }

        }
        if (!isNormalAlert) {
            // Set a negative button and its click listener on alert dialog
            builder.setNegativeButton(if (existingLocale.toLanguageTag() == "en") "Cancel" else "Abbrechen") { dialog, _ ->
                dialog.dismiss()

            }
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun openDialogBox(view: View) {
        val dialog = Dialog(this@DashBoardActivity)

        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_send_info)
        val txt_send_email = dialog.findViewById<TextView>(R.id.txt_send_email)
        val txt_send_sms = dialog.findViewById<TextView>(R.id.txt_send_sms)

        dialog.show()
        txt_send_sms.setOnClickListener {
            requestPermission()
            dialog.dismiss()
        }
        txt_send_email.setOnClickListener {
            if (guest.guest_email.isEmpty()) {
                showMessageOK(getString(R.string.customer_email_address_empty_text_string), true, "")
                dialog.dismiss()
                return@setOnClickListener
            }
            if (databaseHandler.checkStoreExistorNot()) {
                store = databaseHandler.getStore()!!

            } else {
                showMessageOK(getString(R.string.store_smail_address_empty_text_string), true, "")
                dialog.dismiss()
                return@setOnClickListener
            }

            if (getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_GMAIL_PASSWORD, "").isNullOrEmpty()) {
                val passwordDialog = PasswordDialog()
                passwordDialog.show(supportFragmentManager, "")
                dialog.dismiss()
            } else {
                // val (redeem, reward, stamp) = databaseHandler.getGuestTransactionTotal(guest.guest_id.toInt())
                val guestTransactionmodel = databaseHandler.getGuestTransactionBYGuestID(guest.guest_id.toInt())
                if (guestTransactionmodel != null) {
                    redeems = guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM.toInt()
                    rewards = guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD
                    stamps = guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP
                }

                if (databaseHandler.checkVoucherInformationExistorNot(guest.guest_id)) {
                    voucher = databaseHandler.getVoucherInformation(guest.guest_id)!!
                    voucher_value = voucher.voucher_value.toFloat()
                }

                showMessageOK(getString(R.string.send_email_alert_text_string), true, "sendEmail")
                dialog.dismiss()
            }
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE),
                    PERMISSION_REQUEST_CODE)
        } else {
            val guestTransactionmodel = databaseHandler.getGuestTransactionBYGuestID(guest.guest_id.toInt())
            if (guestTransactionmodel != null) {
                redeems = guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM.toInt()
                rewards = guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD
                stamps = guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP
            }

            if (databaseHandler.checkVoucherInformationExistorNot(guest.guest_id)) {
                voucher = databaseHandler.getVoucherInformation(guest.guest_id)!!
                voucher_value = voucher.voucher_value.toFloat()
            }
            val message = getString(R.string.email_template_text_string) + "\n" + getString(R.string.email_template_voucher_value_text_string) + "  = $voucher_value " + getString(R.string.euro_symbol) + "\n" + getString(R.string.email_template_total_premium_text_string) + "  = $rewards\n" + getString(R.string.email_template_total_stamp_text_string) + "  = $stamps"
            if (!guest.guest_mobile_number.isNullOrEmpty()) {
                sendSMS(guest.guest_mobile_number, message)
            } else {
                Toast.makeText(this, getString(R.string.customer_mobile_number_not_set_text_string), Toast.LENGTH_SHORT).show()
            }

        }


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(applicationContext, "Permission Granted", Toast.LENGTH_SHORT).show()
                val guestTransactionmodel = databaseHandler.getGuestTransactionBYGuestID(guest.guest_id.toInt())
                if (guestTransactionmodel != null) {
                    redeems = guestTransactionmodel.KEY_GUEST_TRANSACTION_REDEEM.toInt()
                    rewards = guestTransactionmodel.KEY_GUEST_TRANSACTION_REWARD
                    stamps = guestTransactionmodel.KEY_GUEST_TRANSACTION_STAMP
                }

                if (databaseHandler.checkVoucherInformationExistorNot(guest.guest_id)) {
                    voucher = databaseHandler.getVoucherInformation(guest.guest_id)!!
                    voucher_value = voucher.voucher_value.toFloat()
                }
                val message = getString(R.string.email_template_text_string) + "\n" + getString(R.string.email_template_voucher_value_text_string) + "  = $voucher_value " + getString(R.string.euro_symbol) + "\n" + getString(R.string.email_template_total_premium_text_string) + "  = $rewards\n" + getString(R.string.email_template_total_stamp_text_string) + "  = $stamps"
                if (!guest.guest_mobile_number.isNullOrEmpty()) {
                    sendSMS(guest.guest_mobile_number, message)
                } else {
                    Toast.makeText(this, getString(R.string.customer_mobile_number_not_set_text_string), Toast.LENGTH_SHORT).show()

                }
            } else {
                Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        showMessageOKCancel("You need to allow access permissions",
                                DialogInterface.OnClickListener { _, _ ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermission()
                                    }
                                })
                    }
                }

            }
        }

    }

    @SuppressLint("SimpleDateFormat")
    override fun onBackPressed() {
        val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
        val currentDate = sdf.format(Date())
        databaseHandler.updateGuestLastVisitDate(Guest(currentDate), guest.guest_id.toInt())
        val intent = Intent(this, DrawerActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    // send SMS
    fun sendSMS(phoneNo: String, msg: String) {
        try {
            val smsManager = SmsManager.getDefault()
            //  smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            // convert german language
            val arrSMS = smsManager.divideMessage(msg)
            smsManager.sendMultipartTextMessage(phoneNo, null, arrSMS, null, null)
            Toast.makeText(applicationContext, getString(R.string.text_sms_text_string), Toast.LENGTH_LONG).show()
        } catch (ex: Exception) {
            Toast.makeText(applicationContext, ex.message.toString(), Toast.LENGTH_LONG).show()
            ex.printStackTrace()
        }

    }

    /// send Email
    class SendEmailTask(var redeem: Int,
                        var reward: Int,
                        var stamp: Int,
                        var vouchre_value: Float,
                        var context: Context,
                        var store_address: String,
                        var guest_email: String,
                        var gmail_pwd: String) : AsyncTask<Void, Void, Void>() {


        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void): Void? {
            try {
                if (!gmail_pwd.isNullOrEmpty()) {
                    val sender = GMailSender(
                            store_address,
                            gmail_pwd)

                    sender.sendMail(context.getString(R.string.email_template_subject_text_string),
                            context.resources.getString(R.string.email_template_text_string) + "\n" +
                                    context.resources.getString(R.string.email_template_voucher_value_text_string) + "  = $vouchre_value " + context.resources.getString(R.string.euro_symbol) + "\n" + context.resources.getString(R.string.email_template_total_premium_text_string) + "  = $reward\n" + context.resources.getString(R.string.email_template_total_stamp_text_string) + "  = $stamp",
                            store_address,
                            guest_email)
                } else {
                    Toast.makeText(context, context.getString(R.string.gmail_pwd_alert_text_string), Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {

            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            Toast.makeText(context, context.getString(R.string.email_sent_toast_text_string), Toast.LENGTH_SHORT).show()
        }
    }
}
