package com.gph.scanner.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler

import com.gph.scanner.R
import com.gph.scanner.model.LoginRequirement
import com.gph.scanner.utils.ForegroundTaskDelegate

/**
 * Created by asus on 03-Aug-18.
 */
class SplashActivity :SActivity() {


    private var loadLoginRequirementDelegate: ForegroundTaskDelegate<*>? = null
    val REQUEST_PERMISSION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.layout_splash)
        loadLoginRequirementDelegate = LoadLoginRequirementDelegate(this)
        listOfForegroundTaskDelegates.add(loadLoginRequirementDelegate as LoadLoginRequirementDelegate)


        val handler = Handler()
        handler.postDelayed(Runnable {
            // Actions to do after 3 seconds
            getScannerApplication() .getscannerFunctionService()!!.loadLoginRequirement(loadLoginRequirementDelegate as LoadLoginRequirementDelegate)
        }, 3000)


    }


    private fun gotoWelcomeScreen() {
        val intent = Intent(this, SigninActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun gotoHomeScreen() {
        val intent = Intent(this, DrawerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private class LoadLoginRequirementDelegate(activity: SActivity) : ForegroundTaskDelegate<Int>(activity) {

        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)

            val activity = activityWeakReference.get() as SplashActivity
            if (activity != null && !activity.isFinishing) {
                if (result === LoginRequirement.LOGIN.ordinal) {
                    activity.gotoWelcomeScreen()
                } else {
                    activity.gotoHomeScreen()
                }
            }
        }
    }
}