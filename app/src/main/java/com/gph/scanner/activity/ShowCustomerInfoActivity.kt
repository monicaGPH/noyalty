package com.gph.scanner.activity

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle

import android.view.View
import com.gph.scanner.R
import com.gph.scanner.model.GuestActivity
import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.layout_existing_user.*
import java.text.SimpleDateFormat
import java.util.*

import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem

import android.widget.LinearLayout
import android.widget.Toast
import com.gph.scanner.adapter.ViewAdapter
import com.gph.scanner.fragment.AppInfoDialogFragment
import com.gph.scanner.model.Guest
import com.gph.scanner.model.GuestTransaction
import com.gph.scanner.utils.AppConstant
import com.orhanobut.logger.Logger


/**
 * Created by asus on 13-Aug-18.
 */
class ShowCustomerInfoActivity : SActivity() {
    lateinit var guest_cardid: String
    lateinit var guestActivity: GuestActivity
    lateinit var currentDate: String
    var stampValue: Int = 0
    lateinit var guest: Guest
    lateinit var guest_card_type: String
    var count: Int = 0
    var count_awarded_rewards: Int = 0
    lateinit var dataBaseHandler: DatabaseHandler
    lateinit var layout: LinearLayout
    var count_credit: Int = 0
    var size: Int = 0
    lateinit var existingLocale: Locale
    lateinit var role: String
    private var appInfoDialogfragment: AppInfoDialogFragment? = null

    //crating an arraylist to store users using the data class user
    val datas = ArrayList<String>()
    var adapter = ViewAdapter(datas)


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // add layout
        setContentView(R.layout.layout_existing_user)
        // set custom action bar Name
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.dashboard_text_string)
        // get default or existing language of app
        existingLocale = currentLanguage

        // create DB instance
        dataBaseHandler = DatabaseHandler(this)
        // get stamp Value
        stampValue = getScannerApplication().getAppPreferences()!!.getInt(AppConstant.PREF_KEY_STAMPVALUE, 0)
        //check if stamp value is 0
        if (stampValue == 0) {
            //set default value of stamp is 5
            stampValue = 5
        }
        // get role of loggedinUser
        role = getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERRIGHT, "")
        //adding a layoutmanager
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        layout = LinearLayout(applicationContext)

        if (intent != null) {
            guest_cardid = intent.getStringExtra("INTENT_EXTRA_GUEST_CARD_ID")
            guest_card_type = intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE")
        }
        guest = if (guest_card_type == "NFC") {
            dataBaseHandler.getGuestBYnfcId(guest_cardid)!!
        } else {
            dataBaseHandler.getGuestBYcardId(guest_cardid)!!
        }

        // get current time and date in predefined format
        val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
        currentDate = sdf.format(Date())
        // check guestActivity exist or not
        if (!dataBaseHandler.checkGuestActivityExistorNot(guest.guest_id.toInt())) {
            dataBaseHandler.addGuestActivity(GuestActivity(
                    "",
                    currentDate,
                    guest.guest_id.toString(),
                    "0",
                    "0",
                    "0"))
        }
        // get guestActivity'data
        guestActivity = dataBaseHandler.getGuestActivityeByGuestID(guest.guest_id.toInt())!!

        txt_first_name.text = if (guest.guest_name.isNullOrEmpty()) getString(R.string.customer_name_not_set_text_string) else guest.guest_name
        txt_card_id.text = guest_cardid
        txt_add_stamp.text = "0"
        txt_rewards_available.text = guestActivity.credit
        txt_last_visit.text = guest.guest_created_at
        // check lifetime stamp value
        if (guestActivity.lifetime_stamp == "0") {
            txt_lifetime_stamp.text = "0"

        } else {
            txt_lifetime_stamp.text = ""
            txt_lifetime_stamp.text = guestActivity.lifetime_stamp
            // draw circle image on basis of value of lifetimestamp
            for (i in 1..guestActivity.lifetime_stamp.toInt()) {
                createRewardImage(i)
            }
        }

        count_credit = guestActivity.credit.toInt()
        // check reward-available of not
        val txt_reward_count = txt_rewards_available.text.toString().toInt()
        // draw rewardLayout
        if (txt_reward_count > 0) {
            for (i in 1..txt_reward_count) {
                createRewardLayout()
            }
            fab_stamp.visibility = View.VISIBLE
        } else {
            txt_no_rewards.visibility = View.VISIBLE
        }

        // btn_add_stamp click
        btn_add_stamp.setOnClickListener {
            btn_minus_stamp.isEnabled = true
            val count: Int = txt_add_stamp.text.toString().toInt()
            txt_add_stamp.text = (count + 1).toString()
            layout8_1.visibility = View.VISIBLE
            val stamp = txt_add_stamp.text.toString().toInt() + txt_lifetime_stamp.text.toString().toInt()

            if (txt_add_stamp.text.toString().toInt() != 0 &&
                    (stamp % stampValue) == 0) {
                txt_no_rewards.visibility = View.GONE
                createRewardLayout()
                count_credit += 1
                count_awarded_rewards += 1
                txt_rewards_available.text = (count_credit).toString()
            }
            fab_stamp.visibility = View.VISIBLE
            createRewardImage(txt_add_stamp.text.toString().toInt() + txt_lifetime_stamp.text.toString().toInt())
        }
        var check = 0
        var checks = 0
        // btn_delete_stamp click
        btn_minus_stamp.setOnClickListener {

            val count: Int = txt_add_stamp.text.toString().toInt()
            txt_add_stamp.text = (count - 1).toString()
            deleteRewardImage(txt_add_stamp.text.toString().toInt() + txt_lifetime_stamp.text.toString().toInt())
            val stamp = txt_add_stamp.text.toString().toInt() + txt_lifetime_stamp.text.toString().toInt()


            if (txt_add_stamp.text.toString().toInt() <= 0) {
                btn_minus_stamp.isEnabled = false

                val txt_reward_count = txt_rewards_available.text.toString().toInt()
                if (txt_reward_count == 0) {
                    fab_stamp.visibility = View.GONE
                    layout8_1.visibility = View.GONE
                    txt_no_rewards.visibility = View.VISIBLE
                } else {
                    if ((txt_reward_count - 1) < adapter.sizes()) {
                        Logger.d("Inside ......")
                        adapter.removeItemAtPosition(adapter.sizes() - 1)
                        count_credit -= 1
                        txt_rewards_available.text = (count_credit).toString()
                    } else {
                        txt_no_rewards.visibility = View.GONE
                    }
                }
                return@setOnClickListener
            } else {
                btn_minus_stamp.isEnabled = true
                fab_stamp.visibility = View.VISIBLE


                if (txt_add_stamp.text.toString().toInt() != 0 &&
                        (stamp % stampValue) != 0 && (stamp / stampValue) >= 0) {
                    //  txt_no_rewards.visibility = View.GONE
                    check = (stamp / stampValue)

                    if (checks != check) {
                        checks = check
                        Logger.d(checks)
                        Logger.d(guestActivity.credit.toInt() - adapter.sizes())
                        Logger.d(adapter.sizes())
                        if ((if (guestActivity.credit.toInt() == 0) adapter.sizes() else adapter.sizes() - guestActivity.credit.toInt()) > checks) {
                            count_awarded_rewards -= 1
                            adapter.removeItemAtPosition(checks)
                            count_credit -= 1
                            txt_rewards_available.text = (count_credit).toString()
                        }
                    }
                }
                if (txt_add_stamp.text.toString().toInt() != 0 &&
                        (stamp % stampValue) != 0 && (stamp / stampValue) == 0) {
                    Logger.d(count_awarded_rewards)
                    if ((count_awarded_rewards > 0)) {
                        count_awarded_rewards -= 1
                        adapter.removeItemAtPosition(adapter.sizes() - 1)
                        count_credit -= 1
                        txt_rewards_available.text = (count_credit).toString()
                    }
                }
            }
        }

        // stamp_add_fab click
        fab_stamp.setOnClickListener {

            if (ViewAdapter.countRedeem > 0) {
                if (existingLocale.toLanguageTag() == "en")
                    showMessageOK("You are about to : \n - " + txt_add_stamp.text.toString().trim() + " stamps to give \n - " + ViewAdapter.countRedeem + " bonus redeemed", true)
                else
                    showMessageOK("Sie sind dabei : \n - " + txt_add_stamp.text.toString().trim() + " Stempel zu vergeben \n - " + ViewAdapter.countRedeem + "  Prämie eingelöst", true)
            } else {
                if (existingLocale.toLanguageTag() == "en")
                    showMessageOK("You are about to : \n - " + txt_add_stamp.text.toString().trim() + " stamps to give", false)
                else {
                    showMessageOK("Sie sind dabei :\n - " + txt_add_stamp.text.toString().trim() + " Stempel zu vergeben", false)
                }
            }
        }

    }

    // end of onCreate()

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onResume() {
        super.onResume()


    }

    @Suppress("DEPRECATION")
    fun createRewardImage(rewardPoints: Int) {

        when (rewardPoints % 10) {
            1 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            2 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            3 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            4 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            5 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            6 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            7 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            8 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            9 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            }
            0 -> {
                img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_8.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_9.setColorFilter(applicationContext.resources.getColor(R.color.white))
                img_10.setColorFilter(applicationContext.resources.getColor(R.color.white))
            }

        }

    }

    @Suppress("DEPRECATION")
    fun deleteRewardImage(rewardPoints: Int) {
        if (rewardPoints == 0) {
            img_1.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_2.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_3.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
            img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))

        } else {

            when (rewardPoints % 10) {
                1 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))

                }
                2 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                3 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                4 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                5 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                6 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                7 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                8 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                9 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.grey))
                }
                0 -> {
                    img_1.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_2.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_3.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_4.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_5.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_6.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_7.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_8.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_9.setColorFilter(applicationContext.resources.getColor(R.color.white))
                    img_10.setColorFilter(applicationContext.resources.getColor(R.color.white))
                }

            }
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun createRewardLayout() {
        datas.add(resources.getString(R.string.free_coffee_reard_text))
        //creating our adapter
        adapter = ViewAdapter(datas)
        //now adding the adapter to recyclerview
        recyclerView.adapter = adapter
        recyclerView.adapter.notifyDataSetChanged()
    }


    override fun onDestroy() {
        super.onDestroy()
        count = 0
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showMessageOK(message: String, isRedeem: Boolean) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@ShowCustomerInfoActivity)

        // Set the alert dialog title
        builder.setTitle(resources.getString(R.string.confirmation_alert_text_string))
        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()

            // if reward is redeem
            if (isRedeem) {

                val count_lifetime_reward = guestActivity.lifetime_stamp.toInt() + txt_add_stamp.text.toString().toInt()
                val remainingReward = count_lifetime_reward / stampValue
                val count_redeem_points = (txt_rewards_available.text.toString().toInt()) - ViewAdapter.countRedeem
                Logger.d(ViewAdapter.countRedeem)
                val remainingLifeTimeStamp = count_lifetime_reward % stampValue

                dataBaseHandler.updateGuestActivity(GuestActivity(
                        "",
                        currentDate,
                        "",
                        guestActivity.activity_transactionid,
                        remainingLifeTimeStamp.toString(),
                        count_redeem_points.toString()

                ), guest.guest_id)
                dataBaseHandler.addGuestTransaction(
                        GuestTransaction(
                                guest.guest_id.toInt(),
                                ViewAdapter.countRedeem.toString(),
                                count_redeem_points,
                                remainingLifeTimeStamp.toString().toInt(),
                                getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERNAME, ""),
                                currentDate))
                ViewAdapter.countRedeem = 0
                val intent = Intent(this, DashBoardActivity::class.java)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
                intent.putExtra("INTENT_EXTRA_SCAN_ID", guest_cardid)
                startActivity(intent)
                this.finish()
                this.finish()
                if (existingLocale.toLanguageTag() == "en")
                    Toast.makeText(applicationContext, "Your customer's card is updated", Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(applicationContext, "Die Karte Ihres Kunden wird aktualisiert", Toast.LENGTH_SHORT).show()
            }// if reward is not redeem, only stamp is added
            else {
                val count_lifetime_reward = guestActivity.lifetime_stamp.toInt() + txt_add_stamp.text.toString().toInt()
                val remainingReward = count_lifetime_reward / stampValue
                //   val count_redeem_points = (txt_rewards_available.text.toString().toInt() + remainingReward) - ViewAdapter.countRedeem
                val count_redeem_points = txt_rewards_available.text.toString().toInt() - ViewAdapter.countRedeem
                val remainingLifeTimeStamp = count_lifetime_reward % stampValue
                dataBaseHandler.updateGuestActivity(
                        GuestActivity(
                                "",
                                currentDate,
                                "",
                                guestActivity.activity_transactionid,
                                remainingLifeTimeStamp.toString(),
                                txt_rewards_available.text.toString()
                        ), guest.guest_id)

                dataBaseHandler.addGuestTransaction(
                        GuestTransaction(
                                guest.guest_id.toInt(),
                                ViewAdapter.countRedeem.toString(),
                                count_redeem_points,
                                remainingLifeTimeStamp.toString().toInt(),
                                getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERNAME, ""),
                                currentDate))
                ViewAdapter.countRedeem = 0

                val intent = Intent(this, DashBoardActivity::class.java)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
                intent.putExtra("INTENT_EXTRA_SCAN_ID", guest_cardid)
                startActivity(intent)
                this.finish()
                if (existingLocale.toLanguageTag() == "en")
                    Toast.makeText(applicationContext, "Your customer's card is updated", Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(applicationContext, "Die Karte Ihres Kunden wird aktualisiert", Toast.LENGTH_SHORT).show()

            }
        }
        // Set a negative button and its click listener on alert dialog
        builder.setNegativeButton(if (existingLocale.toLanguageTag() == "en") "CANCEL" else "Abbrechen") { dialog, which ->
            dialog.dismiss()
            txt_add_stamp.text = "0"
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_show_user, menu)
        /*   val shareItem = menu!!.findItem(R.id.action_edit_profile)
           shareItem.isVisible = role == "admin"*/
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_edit_profile -> {
                gotoUpdateUserScreen()
                return true
            }
            R.id.action_app_info -> {
                gotoAppInfoScreen()
                return true
            }

        }

        return true
    }

    fun gotoAppInfoScreen() {

        appInfoDialogfragment = supportFragmentManager.findFragmentByTag(AppInfoDialogFragment.TAG) as AppInfoDialogFragment?
        if (appInfoDialogfragment == null) {
            appInfoDialogfragment = AppInfoDialogFragment.newInstance()
        }
        appInfoDialogfragment!!.show(supportFragmentManager, AppInfoDialogFragment.TAG)

    }

    override fun onBackPressed() {
        ViewAdapter.countRedeem = 0
        val intent = Intent(this, DashBoardActivity::class.java)
        intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
        intent.putExtra("INTENT_EXTRA_SCAN_ID", guest_cardid)
        startActivity(intent)
        this.finish()
    }

    fun gotoUpdateUserScreen() {
        val intent = Intent(this, AddNewCustomerActivity::class.java)
        intent.putExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY", "DashBoard")
        intent.putExtra("INTENT_EXTRA_SCAN_TYPE", guest_card_type)
        intent.putExtra("INTENT_EXTRA_SCAN_ID", guest_cardid)
        Logger.d(guest.guest_id)
        startActivity(intent)
    }

}