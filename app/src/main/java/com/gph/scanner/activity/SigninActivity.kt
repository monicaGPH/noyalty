package com.gph.scanner.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.utils.AppConstant

import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.activity_login.*


/**
 * Created by asus on 03-Aug-18.
 */
class SigninActivity : SActivity() {
    lateinit var dataBaseHandler: DatabaseHandler
    var isClick: Boolean = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onResume() {
        super.onResume()
        val abc = currentLanguage
        if (abc.toLanguageTag() == "en")
            image_flag.setImageResource(R.mipmap.ic_english)
        else
            image_flag.setImageResource(R.mipmap.ic_german)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_login)
        val existingLocale = currentLanguage
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        dataBaseHandler = DatabaseHandler(applicationContext)

        image_flag.setOnClickListener {

            if (existingLocale.toLanguageTag() == "en") {
                image_flag.setImageResource(R.mipmap.ic_german)
                setLanguage("de")
            } else {
                image_flag.setImageResource(R.mipmap.ic_english)
                setLanguage("en")
            }
        }

        txt_reset_pwd.setOnClickListener {
            if (!isClick) {
                et_password.hint = resources.getString(R.string.password_hint)
                et_confirm_pwd.visibility = View.VISIBLE
                btn_signin.visibility = View.GONE
                btn_reset.visibility = View.VISIBLE
                isClick = true
            } else {
                et_confirm_pwd.visibility = View.GONE
                btn_signin.visibility = View.VISIBLE
                btn_reset.visibility = View.GONE
                isClick = false
            }

        }
        btn_reset.setOnClickListener {
            if (!et_password.text.toString().isEmpty() &&
                    !et_confirm_pwd.text.toString().isEmpty() &&
                    et_password.text.toString() == et_confirm_pwd.text.toString() &&
                    !et_username.text.toString().isEmpty()) {
                dataBaseHandler.updateEmployeePwd(et_password.text.toString().trim(), et_username.text.toString().trim())
                Toast.makeText(applicationContext, getString(R.string.pwd_reset_successful_text_string), Toast.LENGTH_SHORT).show()
                et_confirm_pwd.visibility = View.GONE
                btn_signin.visibility = View.VISIBLE
                btn_reset.visibility = View.GONE
            } else {
                if (et_confirm_pwd.text.toString().isEmpty()) {
                    et_confirm_pwd.error = getString(R.string.confirm_pwd_empty_text_string)
                }
                if (et_password.text.toString().trim() != et_confirm_pwd.text.toString().trim()) {
                    showSnakeBar(getString(R.string.pwd_not_match_text_string))
                    et_password.setText("")
                    et_confirm_pwd.setText("")
                }
                if (et_username.text.toString().isEmpty()) {
                    showSnakeBar(getString(R.string.username_empty_error_string))
                }
            }
        }

        btn_signin.setOnClickListener {
            val abc = currentLanguage
            if (!TextUtils.isEmpty(et_username.text.toString().trim()) && !TextUtils.isEmpty(et_password.text.toString().trim())) {

                val check: Boolean = dataBaseHandler.checkEmployeeExistorNot(et_username.text.toString().trim(), et_password.text.toString().trim())
                if (check) {
                    val account = dataBaseHandler.getEmployee(et_username.text.toString().trim(), et_password.text.toString().trim())
                    if (abc.toLanguageTag() == "en")
                        Toast.makeText(applicationContext, "Login Successfull", Toast.LENGTH_SHORT).show()
                    else
                        Toast.makeText(applicationContext, "Anmeldung erfolgreich", Toast.LENGTH_SHORT).show()
                    getScannerApplication().getAppPreferences()?.edit()?.putString(AppConstant.PREF_KEY_USERNAME, et_username.text.toString().trim())?.apply()
                    getScannerApplication().getAppPreferences()?.edit()?.putString(AppConstant.PREF_KEY_PASSWORD, et_password.text.toString().trim())?.apply()
                    if (account != null) {
                        getScannerApplication().getAppPreferences()?.edit()?.putString(AppConstant.PREF_KEY_USERRIGHT, account.emp_right)?.apply()
                    }
                    val intent = Intent(this, DrawerActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                } else {
                    if (abc.toLanguageTag() == "en")
                        showSnakeBar("Incorrect Username and password ")
                    else
                        showSnakeBar("Falscher Benutzername und falsches Passwort")
                }
            } else {
                if (TextUtils.isEmpty(et_username.text.toString().trim())) {
                    if (abc.toLanguageTag() == "en")
                        et_username.error = getString(R.string.username_empty_error_string)
                    else
                        et_username.error = getString(R.string.username_empty_error_string)
                    et_username.requestFocus()
                }
                if (TextUtils.isEmpty(et_password.text.toString().trim())) {
                    if (abc.toLanguageTag() == "en")
                        et_password.error = getString(R.string.pwd_empty_error_String)
                    else
                        et_password.error = getString(R.string.pwd_empty_error_String)
                    et_password.requestFocus()
                }
            }
        }
    }

}