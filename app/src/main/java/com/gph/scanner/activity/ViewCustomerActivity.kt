package com.gph.scanner.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.LinearLayout
import com.gph.scanner.R
import com.gph.scanner.adapter.CustomerAdapter
import com.gph.scanner.model.Guest
import com.gph.scanner.model.GuestActivity
import com.gph.scanner.model.Guest_Info
import com.gph.scanner.model.Voucher
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.layout_view_customer.*
import java.util.*

/**
 * Created by asus on 13-Sep-18.
 */
class ViewCustomerActivity : SActivity() {
    lateinit var dataBaseHandler: DatabaseHandler
    lateinit var adapter: CustomerAdapter
    var customerList = ArrayList<Guest_Info>()
    var scanType: String = ""
    var scanValue: String = ""
    var guest: Guest? = null
    var guestActivity_existing_customer: GuestActivity? = null
    var voucher_existing_customer: Voucher? = null
    var dashboardcall: Boolean = false
    var guestActivity: GuestActivity? = null
    var voucher: Voucher? = null
    lateinit var role: String
    var iscallFromActivity: Boolean = false
    lateinit var existingLocale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_view_customer)

        // set action bar name and set back button functionality
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.customer_information_text_string)

        // database instance
        dataBaseHandler = DatabaseHandler(applicationContext)

        // get role of LoggedIn user
        role = getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERRIGHT, "")

        // get existing language of app
        existingLocale = currentLanguage

        if (intent != null) {
            if (intent.hasExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY"))
                iscallFromActivity = intent.getStringExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY") == "ADDCUSTOMER"
            if (intent.hasExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY_SECONDARY"))
                dashboardcall = intent.getStringExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY_SECONDARY") == "DashboardClass"
        }
        // call from Dashboard activity
        if (dashboardcall) {
            scanType = intent.getStringExtra("INTENT_EXTRA_CARD_TYPE")
            scanValue = intent.getStringExtra("INTENT_EXTRA_CARD_VALUE")

            // get Guest data according to scanValue and scanType
            guest = if (scanType == "NFC") {
                dataBaseHandler.getGuestBYnfcId(scanValue)!!
            } else {
                dataBaseHandler.getGuestBYcardId(scanValue)!!
            }

            if (dataBaseHandler.checkGuestActivityExistorNot(guest!!.guest_id.toInt())) {
                // get GuestActivity data according to GuestId
                guestActivity_existing_customer = dataBaseHandler.getGuestActivityeByGuestID(guest!!.guest_id.toInt())!!
            }

            if (dataBaseHandler.checkVoucherInformationExistorNot(guest!!.guest_id)) {
                // get VoucherInfo data according to GuestId
                voucher_existing_customer = dataBaseHandler.getVoucherInformation(guest!!.guest_id)!!
            }

        }
        recyclerView_customer.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)

        adapter = CustomerAdapter(customerList,// customerList
                this,// context
                iscallFromActivity,// call from ADDCUSTOMER
                scanValue,// NFC or CARD value
                scanType,// card type
                existingLocale,// existing language of app
                guest,// guest instance
                guestActivity_existing_customer,// guestActivity instance
                voucher_existing_customer,// voucher instance
                role,// loggedIn user's right
                this@ViewCustomerActivity,
                getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERNAME, ""))// loggedIn username
        val customers: ArrayList<Guest> = dataBaseHandler.getGuest()!!
        if (customers.size > 0) {
            customers.forEach {
                var stamp_value: String? = null
                var voucher_value: String? = null
                var reward_value: String? = null

                println(it)
                // check guestActivity data exist or not
                if (dataBaseHandler.checkGuestActivityExistorNot(it.guest_id.toInt())) {
                    guestActivity = dataBaseHandler.getGuestActivityeByGuestID(it.guest_id.toInt())!!
                    // get stamp value
                    stamp_value = guestActivity!!.lifetime_stamp
                    // get credit/reward value
                    reward_value = guestActivity!!.credit

                }
                // check voucher data exist or not
                if (dataBaseHandler.checkVoucherInformationExistorNot(it.guest_id)) {
                    voucher = dataBaseHandler.getVoucherInformation(it.guest_id)!!
                    voucher_value = voucher!!.voucher_value
                }
                if (iscallFromActivity) {
                    if ((it.guest_card_id == scanValue || it.guest_nfc_id == scanValue) && customers.size == 1) {

                        showMessageOK(getString(R.string.customer_list_empty_text_string))
                        return@forEach
                    } else {
                        if (!(it.guest_nfc_id == scanValue || it.guest_card_id == scanValue)) {
                            val customer = Guest_Info(it.guest_id,
                                    it.guest_name,
                                    it.guest_add,
                                    it.guest_mobile_number,
                                    it.guest_email,
                                    it.guest_membershipId,
                                    it.guest_nfc_id,
                                    it.guest_card_id,
                                    it.guest_profile_picture,
                                    it.guest_created_at,
                                    if (!voucher_value.isNullOrEmpty()) voucher!!.voucher_value else "0",
                                    if (!stamp_value.isNullOrEmpty()) guestActivity!!.lifetime_stamp else "0",
                                    if (!reward_value.isNullOrEmpty()) guestActivity!!.credit else "0")

                            customerList.add(customer)
                            adapter.notifyDataSetChanged()
                        }

                    }
                } else {
                    val customer = Guest_Info(
                            it.guest_id,
                            it.guest_name,
                            it.guest_add,
                            it.guest_mobile_number,
                            it.guest_email,
                            it.guest_membershipId,
                            it.guest_nfc_id,
                            it.guest_card_id,
                            it.guest_profile_picture,
                            it.guest_created_at,
                            if (!voucher_value.isNullOrEmpty()) voucher!!.voucher_value else "0",
                            if (!stamp_value.isNullOrEmpty()) guestActivity!!.lifetime_stamp else "0",
                            if (!reward_value.isNullOrEmpty()) guestActivity!!.credit else "0"
                    )

                    customerList.add(customer)
                    adapter.notifyDataSetChanged()

                }

            }
            //now adding the adapter to recyclerview
            recyclerView_customer.adapter = adapter

        } else {

            showMessageOK(getString(R.string.customer_empty_list_alert_text_string))
        }
        search_customer.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(arg0: Editable) {
                // TODO Auto-generated method stub
                filter(arg0.toString())
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int,
                                           arg2: Int, arg3: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int,
                                       arg3: Int) {
                // TODO Auto-generated method stub


            }
        })


    }

    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdNames = ArrayList<Guest_Info>()

        //looping through existing elements
        for (customerLists in customerList) {
            //if the existing elements contains the search input

            if (customerLists.guest_name.toLowerCase().contains(text.toLowerCase()) ||
                    customerLists.guest_card_id.toLowerCase().contains(text.toLowerCase()) ||
                    customerLists.guest_nfc_id.toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(customerLists)
            }

        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames)
    }

    fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@ViewCustomerActivity)
        // Set the alert dialog title
        builder.setTitle(getString(R.string.confirmation_alert_text_string))
        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            onBackPressed()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (iscallFromActivity) {

            if (dashboardcall) {
                val intent = Intent(this, DashBoardActivity::class.java)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                startActivity(intent)
                this.finish()
            } else {
                val intent = Intent(this, AddNewCustomerActivity::class.java)
                intent.putExtra("ScanType", scanType)
                intent.putExtra("ScanValue", scanValue)
                setResult(RESULT_OK, intent)
                this.finish()
            }
        } else {
            if (dashboardcall) {
                val intent = Intent(this, DashBoardActivity::class.java)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                startActivity(intent)
                this.finish()
            }
        }

    }
}