package com.gph.scanner.activity

import android.annotation.SuppressLint

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment

import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.MenuItem
import android.view.View

import android.view.WindowManager

import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.model.Guest

import com.gph.scanner.utils.database.DatabaseHandler
import com.orhanobut.logger.Logger
import com.squareup.picasso.Picasso
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import kotlinx.android.synthetic.main.activity_new_customer.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by asus on 11-Aug-18.
 */
class AddNewCustomerActivity : SActivity(), IPickResult {


    private lateinit var dataBaseHandler: DatabaseHandler
    private lateinit var scanType: String
    private lateinit var scanValue: String

    lateinit var guest: Guest
    private var isCallFromdashBoard: Boolean = false
    private var isButtonchecked: Boolean = false
    private var isInformationExist: Boolean = false
    private lateinit var existingLocale: Locale
    private val REQUEST_WRITE_PERMISSION = 786
    private val RESULT_LOAD_IMAGE = 111
    private var photoUrl: String = ""
    private lateinit var dialog: PickImageDialog
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_new_customer)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        hideKeyboard()

        dataBaseHandler = DatabaseHandler(applicationContext)

        val content = SpannableString(getString(R.string.link_to_existing_customer_text_string))
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        txt_link_to_customer.text = content


        existingLocale = currentLanguage

        if (intent != null) {
            if (intent.getStringExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY").equals("DashBoard", true)) {
                scanType = intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE")
                scanValue = intent.getStringExtra("INTENT_EXTRA_SCAN_ID")
                isCallFromdashBoard = true
                guest = if (scanType == "NFC")
                    dataBaseHandler.getGuestBYnfcId(scanValue)!!
                else
                    dataBaseHandler.getGuestBYcardId(scanValue)!!
            } else if (intent.getStringExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY").equals("ScannerClass", true)) {
                if (!TextUtils.isEmpty(intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE"))) {
                    //    isCallFromdashBoard = false
                    scanType = intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE")
                    scanValue = intent.getStringExtra("INTENT_EXTRA_SCAN_ID")
                }
                guest = if (scanType == "NFC")
                    dataBaseHandler.getGuestBYnfcId(scanValue)!!
                else
                    dataBaseHandler.getGuestBYcardId(scanValue)!!

                isCallFromdashBoard = !(guest.guest_name.isEmpty() ||
                        guest.guest_email.isEmpty())
            } else if (intent.getStringExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY").equals("DashboardClass", true)) {

                //    isCallFromdashBoard = false
                scanType = intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE")
                scanValue = intent.getStringExtra("INTENT_EXTRA_SCAN_ID")

                guest = if (scanType == "NFC")
                    dataBaseHandler.getGuestBYnfcId(scanValue)!!
                else
                    dataBaseHandler.getGuestBYcardId(scanValue)!!

                if (!guest.guest_email.isEmpty() || !guest.guest_name.isEmpty()) {
                    isCallFromdashBoard = true
                    isInformationExist = true
                }

            }
        }


        if (!isCallFromdashBoard) {
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

            supportActionBar!!.title = getString(R.string.add_new_customer_text_string)
            btn_addUser.text = getString(R.string.add_String)
            login_title.text = getString(R.string.add_new_customer_text_string)
            txt_link_to_customer.visibility = View.VISIBLE
            login_form.requestFocus()
        } else {
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            if (existingLocale.toLanguageTag() == "en") {
                supportActionBar!!.title = getString(R.string.update_customer_text_string)
                btn_addUser.text = getString(R.string.update_customer_text_string)
                login_title.text = getString(R.string.update_customer_text_string)
            } else {
                supportActionBar!!.title = getString(R.string.update_customer_text_string)
                btn_addUser.text = getString(R.string.update_customer_text_string)
                login_title.text = getString(R.string.update_customer_text_string)
            }
            val guest = dataBaseHandler.getGuestInfoBYGuestID(guest.guest_id.toInt())

            et_name.setText(guest!!.guest_name)
            et_address.setText(guest.guest_add)
            et_email.setText(guest.guest_email)
            et_mobile.setText(guest.guest_mobile_number)
            Logger.d(guest.guest_profile_picture)
            getImage(guest.guest_profile_picture)
            login_form.requestFocus()
            txt_link_to_customer.visibility = View.GONE
        }
        imageView_rounded.setOnClickListener { requestPermission() }

        txt_link_to_customer.setOnClickListener {
            val intent = Intent(applicationContext, ViewCustomerActivity::class.java)
            intent.putExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY", "ADDCUSTOMER")
            intent.putExtra("INTENT_EXTRA_CARD_TYPE", scanType)
            intent.putExtra("INTENT_EXTRA_CARD_VALUE", scanValue)
            startActivityForResult(intent, 1)

        }

        btn_addUser.setOnClickListener {
            var guest_ids: Int = 0
            var guest_card_ids: String = ""
            if (!TextUtils.isEmpty(et_name.text.toString().trim())) {
                isButtonchecked = true
                if (isCallFromdashBoard) {
                    dataBaseHandler.updateGuest(Guest(
                            et_name.text.toString().trim(),
                            et_address.text.toString().trim(),
                            et_mobile.text.toString().trim(),
                            et_email.text.toString().trim(),
                            photoUrl), guest.guest_id.toInt())

                    et_name.text.clear()
                    et_address.text.clear()
                    et_email.text.clear()
                    et_mobile.text.clear()
                    this.finish()
                    hideKeyboard()
                    if (existingLocale.toLanguageTag() == "en")
                        Toast.makeText(applicationContext, "User Information Updated successfully", Toast.LENGTH_SHORT).show()
                    else
                        Toast.makeText(applicationContext, "Benutzerinformationen erfolgreich aktualisiert", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, DashBoardActivity::class.java)

                    intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                    intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                    startActivity(intent)
                    et_name.text.clear()
                    et_address.text.clear()
                    et_email.text.clear()
                    et_mobile.text.clear()
                    this.finish()
                } else {
                    if (!TextUtils.isEmpty(scanType) && !TextUtils.isEmpty(scanValue)) {
                        val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
                        val currentDate = sdf.format(Date())

                        dataBaseHandler.updateGuest(Guest(
                                et_name.text.toString().trim(),
                                if (et_address.text.isEmpty()) guest.guest_add else et_address.text.toString().trim(),
                                if (et_mobile.text.isEmpty()) guest.guest_mobile_number else et_mobile.text.toString().trim(),
                                if (et_email.text.isEmpty()) guest.guest_email else et_email.text.toString().trim(),
                                "",
                                scanValue,
                                scanValue,
                                photoUrl,
                                currentDate), scanValue, scanType)
                        if (scanType.equals("NFC", true)) {
                            var (guest_id, guest_card_id) = dataBaseHandler.getGuestIDBYnfcId(scanValue)!!
                            guest_ids = guest_id
                            guest_card_ids = guest_card_id
                        } else if (scanType.equals("CARD", true)) {
                            val (guest_id, guest_card_id) = dataBaseHandler.getGuestIDBYCARDId(scanValue)!!
                            guest_card_ids = guest_card_id
                            guest_ids = guest_id
                        }
                        Logger.d(guest_card_ids)
                        val intent = Intent(this, DashBoardActivity::class.java)

                        intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                        intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                        startActivity(intent)
                        et_name.text.clear()
                        et_address.text.clear()
                        et_email.text.clear()
                        et_mobile.text.clear()
                        this.finish()
                        if (existingLocale.toLanguageTag() == "en")
                            Toast.makeText(applicationContext, "User Information added successfully", Toast.LENGTH_SHORT).show()
                        else
                            Toast.makeText(applicationContext, "Benutzerinformationen erfolgreich hinzugefügt", Toast.LENGTH_SHORT).show()
                        hideKeyboard()
                    }
                }
            } else {
                when {
                    TextUtils.isEmpty(et_name.text.toString().trim()) -> {
                        et_name.error = getString(R.string.name_error_hint_string)
                        et_name.requestFocus()
                    }
                /*    TextUtils.isEmpty(et_mobile.text.toString().trim()) -> {

                        et_mobile.error = getString(R.string.mobile_number_empty_error_string)
                        et_mobile.requestFocus()
                    }
                    TextUtils.isEmpty(et_email.text.toString().trim()) -> {

                        et_email.error = getString(R.string.email_address_empty_error_string)
                        et_email.requestFocus()
                    }
                    TextUtils.isEmpty(et_address.text.toString().trim()) -> {
                        et_address.error = getString(R.string.address_empty_error_string)
                        et_address.requestFocus()
                    }*/
                }
            }
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_PERMISSION)
        } else {
            gotoFullDetail()

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoFullDetail()
        }
    }


    private fun gotoFullDetail() {
        /*    val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image*//*"
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE)*/
        dialog = PickImageDialog.build(PickSetup())
        dialog.show(this)

    }

    override fun onPickResult(p0: PickResult?) {
        if (p0 != null) {
            if (p0.error == null) {
                //If you want the Uri.
                //Mandatory to refresh image from Uri.
                //getImageView().setImageURI(null);

                //Setting the real returned image.
                //getImageView().setImageURI(r.getUri());

                //If you want the Bitmap.
                // getImageView().setImageBitmap(r.getBitmap());
                Logger.d(p0.path)
                photoUrl = p0.path
                getImage(p0.path)
                dialog.dismiss()
                //Image path
                //r.getPath();
            } else {
                //Handle possible errors
                //TODO: do what you have to do with r.getError();
                Toast.makeText(this, p0.error.message, Toast.LENGTH_LONG).show();
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            val selectedImage = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = contentResolver.query(selectedImage,
                    filePathColumn, null, null, null)
            cursor.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            val picturePath = cursor.getString(columnIndex)
            photoUrl = picturePath
            cursor.close()
            Logger.d(photoUrl)
            getImage(picturePath)
            val picturePathCompress = compressImage(picturePath)
            Logger.d("picture path ", "" + picturePathCompress)
            //   val sourceFile = File(picturePathCompress)

        } else if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            scanType = data.getStringExtra("ScanType")
            scanValue = data.getStringExtra("ScanValue")
        }
    }

    fun getImage(bm: String) {
        if (bm != null) {
            Picasso.get()
                    .load("file://" + bm)
                    .placeholder(R.drawable.ic_person) //this is optional the image to display while the url image is downloading//this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imageView_rounded)
        }
    }

    fun compressImage(imageUri: String): String {

        val filePath = getRealPathFromURI(imageUri)
        var scaledBitmap: Bitmap? = null

        val options = BitmapFactory.Options()

        //      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
        //      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true
        var bmp = BitmapFactory.decodeFile(filePath, options)

        var actualHeight = options.outHeight
        var actualWidth = options.outWidth

        //      max Height and width values of the compressed image is taken as 816x612

        val maxHeight = 816.0f
        val maxWidth = 612.0f
        var imgRatio = (actualWidth / actualHeight).toFloat()
        val maxRatio = maxWidth / maxHeight

        //      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight
                actualWidth = (imgRatio * actualWidth).toInt()
                actualHeight = maxHeight.toInt()
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth
                actualHeight = (imgRatio * actualHeight).toInt()
                actualWidth = maxWidth.toInt()
            } else {
                actualHeight = maxHeight.toInt()
                actualWidth = maxWidth.toInt()

            }
        }

        //      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)

        //      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false

        //      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)

        try {
            //          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()

        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

        val canvas = Canvas(scaledBitmap!!)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bmp, middleX - bmp.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))

        //      check the rotation of the image and display it properly
        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)

            val orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0)
            Log.d("EXIF", "Exif: " + orientation)
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90f)
                Log.d("EXIF", "Exif: " + orientation)
            } else if (orientation == 3) {
                matrix.postRotate(180f)
                Log.d("EXIF", "Exif: " + orientation)
            } else if (orientation == 8) {
                matrix.postRotate(270f)
                Log.d("EXIF", "Exif: " + orientation)
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.width, scaledBitmap.height, matrix,
                    true)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        var out: FileOutputStream? = null
        val filename = getFilename()
        try {
            out = FileOutputStream(filename)

            //          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        return filename

    }

    fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "MyFolder/Images")
        if (!file.exists()) {
            file.mkdirs()
        }


        //String uriSting = "CBIMAGE_" + System.currentTimeMillis() + ".jpg";
        return file.path + File.separator + "NIMAGE_" + System.currentTimeMillis() + ".png"

    }

    private fun getRealPathFromURI(contentURI: String): String {
        val contentUri = Uri.parse(contentURI)
        val cursor = contentResolver.query(contentUri, null, null, null, null)
        if (cursor == null) {
            return contentUri.path
        } else {
            cursor.moveToFirst()
            val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            return cursor.getString(index)
        }
    }

    fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }

        return inSampleSize
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@AddNewCustomerActivity)

        // Set the alert dialog title
        builder.setTitle(resources.getString(R.string.app_name))

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton(if (existingLocale.toLanguageTag() == "en") "OK" else "OK") { dialog, _ ->
            dialog.dismiss()
            if (!isCallFromdashBoard) {
                hideKeyboard()

                val intent = Intent(this, DashBoardActivity::class.java)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                startActivity(intent)
                this.finish()
            } else {
                if (isInformationExist) {
                    hideKeyboard()
                    val intent = Intent(this, DashBoardActivity::class.java)
                    intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
                    intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
                    startActivity(intent)
                    this.finish()
                } else {
                    hideKeyboard()
                    this.finish()
                }
            }
        }
        // Set a negative button and its click listener on alert dialog
        builder.setNegativeButton(if (existingLocale.toLanguageTag() == "en") "Cancel" else "Abbrechen") { dialog, _ ->
            dialog.dismiss()

        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBackPressed() {
        if (!isButtonchecked) {
            if (existingLocale.toLanguageTag() == "en")
                showMessageOK("user data not yet saved, Do you want to abort, stop?")
            else
                showMessageOK("Benutzerdaten noch nicht gespeichert, möchten Sie abbrechen?")
        } else {
            this.finish()
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}