package com.gph.scanner.activity

import android.content.Context
import android.content.pm.ActivityInfo

import android.os.Bundle

import android.support.design.widget.Snackbar

import android.view.View
import android.view.inputmethod.InputMethodManager
import butterknife.ButterKnife
import com.akexorcist.localizationactivity.core.OnLocaleChangedListener
import com.gph.scanner.R
import com.gph.scanner.ScannerApplication
import com.gph.scanner.service.ScannerFunctionService
import com.gph.scanner.utils.ForegroundTaskDelegate
import com.gph.scanner.utils.ScannerProgressDialogFragment

import kotlin.collections.ArrayList


import com.akexorcist.localizationactivity.ui.LocalizationActivity
import java.util.regex.Pattern


/**
 * Created by asus on 03-Aug-18.
 */
open class SActivity : LocalizationActivity(),OnLocaleChangedListener{


    open lateinit var rootLayout: View
    open lateinit var listOfForegroundTaskDelegates: MutableList<ForegroundTaskDelegate<*>>
    open lateinit var scannerFunctionService: ScannerFunctionService
    open var onStartCount = 0
    open val TAG_PROGRESS_DIALOG = "progressDialog"
    //private val localizationDelegate = LocalizationActivityDelegate(this)
    open val TAG_ALERT_DIALOG = "alertDialog"


    /*
* show message when internet is not available
*/
    fun showSnakeBar(text:String) {
        val parentLayout = findViewById<View>(android.R.id.content)
        Snackbar.make(parentLayout,text , Snackbar.LENGTH_LONG)
                .setAction("CLOSE") { }
                .setActionTextColor(resources.getColor(android.R.color.holo_red_light))
                .show()

    }

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /* localizationDelegate.addOnLocaleChangedListener(this)
         localizationDelegate.onCreate(savedInstanceState);*/
         requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        onStartCount = 1
        if (savedInstanceState == null) {   // 1st time
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left)
        }   // already created so reverse animation
        else {
            onStartCount = 2
        }

        listOfForegroundTaskDelegates = ArrayList()

         rootLayout = layoutInflater.inflate(R.layout.holder_empty, null)
        setContentView(rootLayout)

        ButterKnife.bind(this)

        scannerFunctionService = getScannerApplication().getscannerFunctionService()!!
    }

  /*  override fun onResume() {
        super.onResume()
        localizationDelegate.onResume(this)
    }*/

    override fun onStart() {
        super.onStart()
        if (onStartCount > 1) {
            this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right)
        } else if (onStartCount == 1) {
            onStartCount++
        }
    }

 /*   override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(newBase))
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }

    override fun getResources(): Resources {
        return localizationDelegate.getResources(super.getResources())
    }

    fun setLanguage(language: String) {
        localizationDelegate.setLanguage(this, language)
    }

    fun setLanguage(locale: Locale) {
        localizationDelegate.setLanguage(this, locale)
    }

    fun setDefaultLanguage(language: String) {
        localizationDelegate.setDefaultLanguage(language)
    }

    fun setDefaultLanguage(locale: Locale) {
        localizationDelegate.setDefaultLanguage(locale)
    }

    fun getCurrentLanguage(): Locale {
        return localizationDelegate.getLanguage(this)
    }*/
     override fun onDestroy() {
        for (delegate in listOfForegroundTaskDelegates) {
            delegate.cancel()
        }

        super.onDestroy()
    }

   open fun getScannerApplication(): ScannerApplication {
        return application as ScannerApplication
    }



    fun hideKeyboard() {
        val im = this.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = window.decorView
        im.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun showKeyboard() {
        val im = this.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = window.decorView
        im.showSoftInput(view.windowToken as View, InputMethodManager.SHOW_IMPLICIT)

    }

    fun showProgressDialog() {
        val ft = supportFragmentManager.beginTransaction()
        val prevFrag = supportFragmentManager.findFragmentByTag(TAG_PROGRESS_DIALOG)
        if (prevFrag == null) {
            val newFrag = ScannerProgressDialogFragment.newInstance()
            ft.add(newFrag, TAG_PROGRESS_DIALOG)
            ft.commitAllowingStateLoss()

        } else {
            ft.remove(prevFrag)
            ft.commitAllowingStateLoss()
            supportFragmentManager.popBackStackImmediate()

        }


    }

    fun dismissProgressDialog() {
        val ft = supportFragmentManager.beginTransaction()
        val prevFrag = supportFragmentManager.findFragmentByTag(TAG_PROGRESS_DIALOG)
        if (prevFrag != null) {
            ft.remove(prevFrag)
            val dlgFrag = prevFrag as ScannerProgressDialogFragment
            if (dlgFrag!!.dialog != null && dlgFrag!!.dialog.isShowing && dlgFrag!!.isResumed) {
                dlgFrag.dismissAllowingStateLoss()
                supportFragmentManager.popBackStackImmediate()
            }
        }
        ft.commitAllowingStateLoss()
    }
    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


}