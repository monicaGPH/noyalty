package com.gph.scanner.activity

import android.annotation.TargetApi
import android.app.AlertDialog

import android.content.Intent
import android.os.Build
import android.os.Bundle

import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View

import com.gph.scanner.R
import com.gph.scanner.adapter.DrawerAdapter
import com.gph.scanner.fragment.*
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.ForegroundTaskDelegate
import com.orhanobut.logger.Logger
import java.util.*


/**
 * Created by asus on 07-Aug-18.
 */
class DrawerActivity : SActivity(), DrawerAdapter.OnItemClickListener {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onClick(view: View, position: Int) {
        com.orhanobut.logger.Logger.i("onClick")
        selectItem(position)
    }

    private var drawerOptions = arrayOfNulls<String>(10)

    private lateinit var logOutDelegate: LogoutDelegate
    lateinit var drawerLayout: DrawerLayout
    lateinit var role: String
    private lateinit var leftDrawer: RecyclerView
    private lateinit var existingLocale: Locale
    private var drawerToggle: ActionBarDrawerToggle? = null


    @Suppress("UNCHECKED_CAST")
    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_drawer)

        role = getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERRIGHT, "")
        Logger.e(role)
        logOutDelegate = LogoutDelegate(this)
        listOfForegroundTaskDelegates.add(logOutDelegate)

        existingLocale = currentLanguage

        drawerLayout = findViewById(R.id.drawer_layout)
        leftDrawer = findViewById(R.id.left_drawer)
        drawerOptions = if (role == "admin") {
            resources.getStringArray(R.array.drawer_options_array_admin)
        } else {
            resources.getStringArray(R.array.drawer_options_array_normal)
        }
        leftDrawer.setHasFixedSize(true)
        leftDrawer.adapter = DrawerAdapter(drawerOptions as Array<String>, this)

        drawerToggle = object : ActionBarDrawerToggle(
                this, /* host Activity */
                drawerLayout, /* DrawerLayout object */
                R.string.drawer_open, /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            override fun onDrawerClosed(drawerView: View) {
                invalidateOptionsMenu()
                supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_drawer_icon)
            }

            override fun onDrawerOpened(drawerView: View) {
                invalidateOptionsMenu()
                supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_back)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, 0f)
            }
        }

        if (savedInstanceState == null) {
            selectItem(0)
        }


        drawerLayout.addDrawerListener(drawerToggle as ActionBarDrawerToggle)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_drawer_icon)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return drawerToggle!!.onOptionsItemSelected(item)
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun selectItem(position: Int) {
        var fragment: Fragment? = null
        if (role == "admin") {
            when (position) {
                0 -> fragment = ScanFragment()
                1 -> fragment = AddStoreFragment()
                2 -> fragment = ViewLogsfragment()
                3 -> fragment = AddNewUserFragment()
                4 -> fragment = SettingFragment()
                5 -> {
                    if (existingLocale.toLanguageTag() == "en")
                        showMessageOK("Do you really want to signout from app ?")
                    else
                        showMessageOK("Möchten Sie sich wirklich von der App abmelden?")
                }
                else -> {
                }
            }

        } else {
            when (position) {
                0 -> fragment = ScanFragment()
                1 -> fragment = ViewLogsfragment()
                2 -> fragment = SettingFragment()
                3 -> {
                    if (existingLocale.toLanguageTag() == "en")
                        showMessageOK("Would you like yourself log out?")
                    else
                        showMessageOK("Möchten Sie sich ausloggen?")
                }
                else -> {
                }
            }
        }

        if (fragment != null) {

            Logger.i(drawerOptions[position].toString())

            supportActionBar!!.title = drawerOptions[position]
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.content_frame, fragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

        drawerLayout.closeDrawer(leftDrawer)
    }

    override fun onBackPressed() {
        if (fragmentManager.backStackEntryCount == 0) {
            this.finish()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle!!.syncState()
    }

    private fun logout() {
        scannerFunctionService.logout(logOutDelegate)
    }

    private inner class LogoutDelegate(activity: SActivity) : ForegroundTaskDelegate<Boolean>(activity) {

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            val activity = activityWeakReference.get() as DrawerActivity
            if (!activity.isFinishing) {
                activity.gotoLoginScreen()
            }
        }
    }

    private fun gotoLoginScreen() {
        val intent = Intent(this, SigninActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@DrawerActivity)

        // Set the alert dialog title
        builder.setTitle(resources.getString(R.string.app_name))

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton(if (existingLocale.toLanguageTag() == "en")"Yes" else "Ja") { dialog, which ->
            dialog.dismiss()
            logout()
        }
        // Set a negative button and its click listener on alert dialog
        builder.setNegativeButton(if (existingLocale.toLanguageTag() == "en")"No" else "Nein") { dialog, which ->
            dialog.dismiss()

        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }



}