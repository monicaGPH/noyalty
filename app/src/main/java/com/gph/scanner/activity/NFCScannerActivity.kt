package com.gph.scanner.activity

import android.app.AlertDialog
import com.gph.scanner.utils.Listener
import android.widget.Toast
import android.nfc.NfcAdapter
import android.content.Intent
import android.app.PendingIntent
import android.content.IntentFilter
import android.nfc.Tag
import android.os.Bundle
import com.gph.scanner.R
import com.gph.scanner.fragment.NFCScannerDialogFragment

import android.nfc.TagLostException
import android.view.MenuItem


/**
 * Created by asus on 07-Aug-18.
 */
class NFCScannerActivity : SActivity(), Listener {

    private var mNfcReadFragment: NFCScannerDialogFragment? = null
    private var isDialogDisplayed = false
    private var isWrite = false
    private var mNfcAdapter: NfcAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.holder_empty)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "NFCScanner"

        initNFC()
    }


    private fun initNFC() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this)
    }


    private fun showReadFragment() {
        mNfcReadFragment = supportFragmentManager.findFragmentByTag(NFCScannerDialogFragment.TAG) as NFCScannerDialogFragment?
        if (mNfcReadFragment == null ) {
            mNfcReadFragment = NFCScannerDialogFragment.newInstance()
        }
        if(!mNfcReadFragment!!.isVisible)
        mNfcReadFragment!!.show(supportFragmentManager, NFCScannerDialogFragment.TAG)
    }

    override fun onDialogDisplayed() {
        isDialogDisplayed = true
    }

    override fun onDialogDismissed() {
        isDialogDisplayed = false
        isWrite = false
    }

    override fun onResume() {
        super.onResume()
        try {
            val tagDetected = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
            val ndefDetected = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
            val techDetected = IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)
            val nfcIntentFilter = arrayOf(techDetected, tagDetected, ndefDetected)

            val pendingIntent = PendingIntent.getActivity(this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
            if (mNfcAdapter != null && mNfcAdapter!!.isEnabled) {
                mNfcAdapter!!.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null)
                showReadFragment()
            } else {
                showMessageOK(getString(R.string.nfc_not_available_text_string))
            }
        } catch (ex: TagLostException) {
            Toast.makeText(applicationContext, ex.toString(), Toast.LENGTH_SHORT).show()
        }

    }

    override fun onPause() {
        super.onPause()
        if (mNfcAdapter != null)
            mNfcAdapter!!.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent) {
        val tag = intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)

        if (tag != null) {
            Toast.makeText(this, getString(R.string.message_tag_detected), Toast.LENGTH_SHORT).show()

            if (isDialogDisplayed) {

                if (isWrite) {
                } else {
                    if (intent.action == NfcAdapter.ACTION_TAG_DISCOVERED) {
                        try {
                            //    ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
                            mNfcReadFragment!!.onNfcDetected(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID))
                            Toast.makeText(applicationContext, "NFC Tag :" + mNfcReadFragment!!.ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)), Toast.LENGTH_SHORT).show()
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            Toast.makeText(applicationContext, "Error :" + ex.stackTrace, Toast.LENGTH_SHORT).show()
                        }
                    }

                }
            }
        }
    }

    private fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@NFCScannerActivity)

        // Set the alert dialog title
        builder.setTitle(resources.getString(R.string.app_name))

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            onBackPressed()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    override fun onBackPressed() {

        val intent = Intent(this, DrawerActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}