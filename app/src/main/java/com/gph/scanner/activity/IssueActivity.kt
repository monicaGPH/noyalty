package com.gph.scanner.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast

import com.gph.scanner.R
import com.gph.scanner.model.Guest
import com.gph.scanner.model.Voucher
import com.gph.scanner.utils.database.DatabaseHandler
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_voucher_screen.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by asus on 03-Oct-18.
 */
class IssueActivity : SActivity() {

    lateinit var databaseHandler: DatabaseHandler
    lateinit var guest_card_id: String
    lateinit var guest_card_value: String
    lateinit var voucher: Voucher
    lateinit var action: String
    lateinit var guest: Guest
    var check: Boolean = false
    var temp_voucher_value = StringBuffer()
    private lateinit var existingLocale: Locale
    lateinit var voucher_value: String

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_screen)
        // set custom action bar Name
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.select_amount_text_string)

        existingLocale = currentLanguage
        txt_amount_value_outer.showSoftInputOnFocus = false
        outer_framelayout.requestFocus()
        //  hideKeyboard()
        txt_amount_value_outer.setSelectAllOnFocus(true)
        if (intent != null) {
            guest_card_id = intent.getStringExtra("INTENT_EXTRA_GUEST_CARD_ID")
            guest_card_value = intent.getStringExtra("INTENT_EXTRA_SCAN_TYPE")
            action = intent.getStringExtra("INTENT_EXTRA_CALL")
        }

        databaseHandler = DatabaseHandler(this)
        guest = if (guest_card_value == "NFC")
            databaseHandler.getGuestBYnfcId(guest_card_id)!!
        else {
            databaseHandler.getGuestBYcardId(guest_card_id)!!
        }

        txt_amount_value_outer.setOnClickListener {
            if (!check) {
                hideKeyboard()
                custom_keyboard_layout.visibility = View.VISIBLE
                check = true
            } else {
                hideKeyboard()
                custom_keyboard_layout.visibility = View.GONE
                check = false
            }
        }

        txt_value1.setOnClickListener {

            temp_voucher_value.append("10")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }

        txt_value2.setOnClickListener {
            temp_voucher_value.append("20")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_value3.setOnClickListener {
            temp_voucher_value.append("50")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }

        txt_key_0.setOnClickListener {
            temp_voucher_value.append("0")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_1.setOnClickListener {
            temp_voucher_value.append("1")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_2.setOnClickListener {
            temp_voucher_value.append("2")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_3.setOnClickListener {
            temp_voucher_value.append("3")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_4.setOnClickListener {
            temp_voucher_value.append("4")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_5.setOnClickListener {
            temp_voucher_value.append("5")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_6.setOnClickListener {
            temp_voucher_value.append("6")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_7.setOnClickListener {
            temp_voucher_value.append("7")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_8.setOnClickListener {
            temp_voucher_value.append("8")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_9.setOnClickListener {
            temp_voucher_value.append("9")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_dot.setOnClickListener {
            temp_voucher_value.append(".")
            txt_amount_value_outer.text = temp_voucher_value.toString()
        }
        txt_key_clear.setOnClickListener {
            if (!temp_voucher_value.isEmpty()) {
                temp_voucher_value.deleteCharAt(temp_voucher_value.length - 1)
                txt_amount_value_outer.text = temp_voucher_value
            }
        }


    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()

        if (!databaseHandler.checkVoucherInformationExistorNot(guest.guest_id)) {
            txt_amount_value.text = "0"
            voucher_value = "0"
        } else {
            voucher = databaseHandler.getVoucherInformation(guest.guest_id)!!
            voucher_value = voucher.voucher_value

            txt_amount_value.text = "" + voucher_value
            Logger.d(voucher_value)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_voucher, menu)
        val shareItem = menu!!.findItem(R.id.action_add)
        if (action == "Issue") {

            shareItem.title = resources.getString(R.string.add_String)

        } else {
            shareItem.title = getString(R.string.reddem_text_string)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_add -> {
                addVoucherValue()
                return true
            }

        }

        return true
    }

    @SuppressLint("SimpleDateFormat")
    private fun addVoucherValue() {
        // get current time and date in predefined format
        val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
        val currentDate = sdf.format(Date())
        if (action == "Issue") {
            if (this.voucher_value == "0") {
                if (!databaseHandler.checkVoucherInformationExistorNot(guest.guest_id)) {
                    databaseHandler.addVoucher(Voucher(
                            guest.guest_id,
                            txt_amount_value_outer.text.toString(),
                            currentDate))
                } else {
                    databaseHandler.updateVoucher(Voucher(
                            txt_amount_value_outer.text.toString(),
                            currentDate),
                            guest.guest_id)
                }
                Toast.makeText(this, getString(R.string.voucher_add_toast_text_string), Toast.LENGTH_SHORT).show()
                hideKeyboard()
                gotoDashBoardActivity()
            } else {

                val updated_voucher_value = voucher_value.toFloat() + txt_amount_value_outer.text.toString().toFloat()
                Logger.d(updated_voucher_value)
                databaseHandler.updateVoucher(Voucher(
                        updated_voucher_value.toString(),
                        currentDate),
                        guest.guest_id)
                Toast.makeText(this, getString(R.string.voucher_add_toast_text_string), Toast.LENGTH_SHORT).show()
                hideKeyboard()
                gotoDashBoardActivity()
            }
        } else {
            if (txt_amount_value_outer.text.toString() != "0") {
                if (txt_amount_value.text != "0") {
                    if (voucher_value.toFloat() > txt_amount_value_outer.text.toString().toFloat()) {
                        val updated_voucher_value: Float = voucher_value.toFloat() - txt_amount_value_outer.text.toString().toFloat()
                        databaseHandler.updateVoucher(Voucher(updated_voucher_value.toString(),
                                currentDate),
                                guest.guest_id)
                        Toast.makeText(this, getString(R.string.voucher_burn_toast_text_string), Toast.LENGTH_SHORT).show()
                        hideKeyboard()
                        gotoDashBoardActivity()
                    } else {
                        showMessageOK(getString(R.string.voucher_insufficient_text_string))
                    }


                } else {
                    showMessageOK(getString(R.string.voucher_balance_empty_text_string))
                }
            }
        }

    }

    private fun gotoDashBoardActivity() {
        val intent = Intent(this, DashBoardActivity::class.java)
        intent.putExtra("ScanType", guest_card_value)
        intent.putExtra("ScanValue", guest_card_id)
        setResult(RESULT_OK, intent)
        this.finish()
    }

    private fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@IssueActivity)
        // Set the alert dialog title
        builder.setTitle(getString(R.string.confirmation_alert_text_string))
        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            onBackPressed()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        hideKeyboard()
        gotoDashBoardActivity()
    }
}