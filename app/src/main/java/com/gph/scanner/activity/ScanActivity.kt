package com.gph.scanner.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.gph.scanner.utils.database.DatabaseHandler
import com.orhanobut.logger.Logger
import me.dm7.barcodescanner.zbar.ZBarScannerView

@SuppressLint("Registered")
/**
 * Created by asus on 06-Aug-18.
 */
class ScanActivity : AppCompatActivity(), ZBarScannerView.ResultHandler {
    private var mScannerView: ZBarScannerView? = null
    lateinit var dataBaseHandler: DatabaseHandler
    private val PERMISSION_REQUEST_CODE = 200

    //camera permission is needed.

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        dataBaseHandler = DatabaseHandler(applicationContext)
    }

    override fun onResume() {
        super.onResume()
        if (checkPermission()) {
            //main logic or main code

            // . write your main code to execute, It will execute if the permission is already given.
            mScannerView = ZBarScannerView(this)    // Programmatically initialize the scanner view
            setContentView(mScannerView)
            mScannerView!!.setAutoFocus(true)  // Register ourselves as a handler
            mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
            mScannerView!!.startCamera()          // Start camera on resume// Set the scanner view as the content view
        } else {

            requestPermission()
        }
    }

    override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    override fun handleResult(result: me.dm7.barcodescanner.zbar.Result) {
        // Do something with the result here
        Logger.e(result.contents) // Prints scan results
        Logger.e(result.barcodeFormat.name) // Prints the scan format (qrcode, pdf417 etc.)

        //    MainActivity.tvresult.setText(result.getContents());

        if (!dataBaseHandler.checkGuestEmployeeExistorNotCard(result.contents)) {
            Toast.makeText(applicationContext, "new user", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(applicationContext, "old user", Toast.LENGTH_SHORT).show()
        }

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }

    private fun checkPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            false
        } else true
    }

    private fun requestPermission() {

        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                PERMISSION_REQUEST_CODE)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show()

                // main logic
                mScannerView = ZBarScannerView(this)    // Programmatically initialize the scanner view
                setContentView(mScannerView)
                mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
                mScannerView!!.startCamera()          // Start camera on resume// Set the scanner view as the content view
            } else {
                Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        showMessageOKCancel("You need to allow access permissions",
                                DialogInterface.OnClickListener { dialog, which ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermission()
                                    }
                                })
                    }
                }

            }
        }

    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@ScanActivity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }
}