package com.gph.scanner.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.gph.scanner.R
import kotlinx.android.synthetic.main.layout_user_information.*

/**
 * Created by asus on 28-Aug-18.
 */
class ViewCustomerInfomationActivity : SActivity() {

    private var dashboardcall: Boolean = false
    lateinit var scanType: String
    lateinit var scanValue: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_user_information)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = resources.getString(R.string.customer_profile_customer_info_string)
        if (intent.hasExtra("KEY_NAME") &&
                intent.hasExtra("KEY_ADD") &&
                intent.hasExtra("KEY_MOBILE") &&
                intent.hasExtra("KEY_EMAIL") &&
                intent.hasExtra("KEY_REDEEM") &&
                intent.hasExtra("KEY_REWARD") &&
                intent.hasExtra("KEY_STAMP")) {
            if (intent.getStringExtra("KEY_NAME").isNullOrEmpty()) name.text = getString(R.string.customer_name_not_set_text_string) else name.text = intent.getStringExtra("KEY_NAME")
            if (intent.getStringExtra("KEY_ADD").isNullOrEmpty()) address.text = getString(R.string.customer_address_not_set_text_string) else address.text = intent.getStringExtra("KEY_ADD")
            if (intent.getStringExtra("KEY_MOBILE").isNullOrEmpty()) mobile.text = getString(R.string.customer_mobile_number_not_set_text_string) else mobile.text = intent.getStringExtra("KEY_MOBILE")
            if (intent.getStringExtra("KEY_EMAIL").isNullOrEmpty()) Email.text = getString(R.string.customer_email_address_not_set_text_string) else Email.text = intent.getStringExtra("KEY_EMAIL")
            redeem.text = intent.getIntExtra("KEY_REDEEM", 0).toString()
            reward.text = intent.getIntExtra("KEY_REWARD", 0).toString()
            stamps.text = intent.getIntExtra("KEY_STAMP", 0).toString()
            voucher.text = intent.getFloatExtra("KEY_VOUCHER", 0F).toString() + " " + resources.getString(R.string.euro_symbol)

        }
        if (intent.hasExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY_SECONDARY")) {
            if (intent.getStringExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY_SECONDARY") == "DashboardClass") {
                dashboardcall = true
                scanType = intent.getStringExtra("INTENT_EXTRA_CARD_TYPE")
                scanValue = intent.getStringExtra("INTENT_EXTRA_CARD_VALUE")
                if (intent.getStringExtra("INTENT_EXTRA_GUEST_NAME").isNullOrEmpty()) name.text = getString(R.string.customer_name_not_set_text_string) else name.text = intent.getStringExtra("INTENT_EXTRA_GUEST_NAME")
                if (intent.getStringExtra("INTENT_EXTRA_GUEST_ADDRESS").isNullOrEmpty()) address.text = getString(R.string.customer_address_not_set_text_string) else address.text = intent.getStringExtra("INTENT_EXTRA_GUEST_ADDRESS")
                if (intent.getStringExtra("INTENT_EXTRA_GUEST_MOBILE").isNullOrEmpty()) mobile.text = getString(R.string.customer_mobile_number_not_set_text_string) else mobile.text = intent.getStringExtra("INTENT_EXTRA_GUEST_MOBILE")
                if (intent.getStringExtra("INTENT_EXTRA_GUEST_EMAIL").isNullOrEmpty()) Email.text = getString(R.string.customer_email_address_not_set_text_string) else Email.text = intent.getStringExtra("INTENT_EXTRA_GUEST_EMAIL")
                redeem.text = intent.getIntExtra("INTENT_EXTRA_GUEST_REWARD_REDEEM", 0).toString()
                reward.text = intent.getIntExtra("INTENT_EXTRA_GUEST_REWARD_AWAILABLE", 0).toString()
                stamps.text = intent.getIntExtra("INTENT_EXTRA_GUEST_STAMP_VALUE", 0).toString()
                voucher.text = intent.getFloatExtra("INTENT_EXTRA_GUEST_VOUCHER", 0F).toString() + " " + resources.getString(R.string.euro_symbol)

            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (dashboardcall) {
            val intent = Intent(this, DashBoardActivity::class.java)
            intent.putExtra("INTENT_EXTRA_SCAN_TYPE", scanType)
            intent.putExtra("INTENT_EXTRA_SCAN_ID", scanValue)
            startActivity(intent)
            this.finish()
        }
    }
}