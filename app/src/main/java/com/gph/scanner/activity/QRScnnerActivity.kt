package com.gph.scanner.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.model.Guest
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.database.DatabaseHandler
import com.orhanobut.logger.Logger
import me.dm7.barcodescanner.zbar.ZBarScannerView
import java.text.SimpleDateFormat

import java.util.*

/**
 * Created by asus on 02-Aug-18.
 */
class QRScnnerActivity : SActivity(), ZBarScannerView.ResultHandler {
    private var mScannerView: ZBarScannerView? = null
    lateinit var dataBaseHandler: DatabaseHandler
    private val PERMISSION_REQUEST_CODE = 200

    //camera permission is needed.

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "CardScanner"
        dataBaseHandler = DatabaseHandler(applicationContext)
    }

    override fun onResume() {
        super.onResume()
        if (checkPermission()) {
            //main logic or main code
            // . write your main code to execute, It will execute if the permission is already given.
            mScannerView = ZBarScannerView(this)    // Programmatically initialize the scanner view
            setContentView(mScannerView)

            //       mScannerView!!.setAutoFocus(true)  // Register ourselves as a handler
            mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
            mScannerView!!.startCamera()
            // Start camera on resume// Set the scanner view as the content view
        } else {

            requestPermission()
        }
    }

    @SuppressLint("SimpleDateFormat")
    override fun handleResult(result: me.dm7.barcodescanner.zbar.Result) {
        // Do something with the result here
        Logger.e(result.contents) // Prints scan results
        Logger.e(result.barcodeFormat.name) // Prints the scan format (qrcode, pdf417 etc.)

        //    MainActivity.tvresult.setText(result.getContents());

        if (!dataBaseHandler.checkGuestEmployeeExistorNotCard(result.contents)) {

                val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
                val currentDate = sdf.format(Date())


                dataBaseHandler.addGuest(com.gph.scanner.model.Guest(
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        result.contents,
                        "",
                        currentDate))
             /*   val intent = Intent(this, AddNewCustomerActivity::class.java)
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", "CARD")
                intent.putExtra("INTENT_EXTRA_SCAN_ID", result.contents)
                intent.putExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY", "ScannerClass")
                startActivity(intent)
                finish()*/
        }
        val guest: Guest? = dataBaseHandler.getGuestBYcardId(result.contents)
        val intent = Intent(this, DashBoardActivity::class.java)
        if (guest != null) {
            intent.putExtra("INTENT_EXTRA_SCAN_TYPE", "CARD")
            intent.putExtra("INTENT_EXTRA_SCAN_ID", result.contents)
        }
        startActivity(intent)
        finish()
        /*else {
            Toast.makeText(applicationContext, "old user", Toast.LENGTH_SHORT).show()
            val guest: Guest? = dataBaseHandler.getGuestBYcardId(result.contents)
            val intent = Intent(this, DashBoardActivity::class.java)
            if (guest != null) {
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", "CARD")
                intent.putExtra("INTENT_EXTRA_SCAN_ID", result.contents)
            }
            startActivity(intent)
            finish()
        }*/

    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {

        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                PERMISSION_REQUEST_CODE)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(applicationContext, "Permission Granted", Toast.LENGTH_SHORT).show()

                // main logic
                mScannerView = ZBarScannerView(this)    // Programmatically initialize the scanner view
                setContentView(mScannerView)
                if (mScannerView != null) {
                    mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
                    mScannerView!!.startCamera()          // Start camera on resume// Set the scanner view as the content view
                }
            } else {
                Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        showMessageOKCancel("You need to allow access permissions",
                                DialogInterface.OnClickListener { dialog, which ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermission()
                                    }
                                })
                    }
                }

            }
        }

    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@QRScnnerActivity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }

    override fun onBackPressed() {
        val intent = Intent(this, DrawerActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
/*
    public override fun onDestroy() {
        super.onDestroy()

        mScannerView = null
        mScannerView!!.stopCamera()

    }*/
}