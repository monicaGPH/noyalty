package com.gph.scanner.activity

import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.LinearLayout
import com.gph.scanner.R
import com.gph.scanner.adapter.UserAdapter
import com.gph.scanner.model.UserModel
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.database.DatabaseHandler
import java.util.*
import kotlinx.android.synthetic.main.layout_user_list.*
import kotlinx.android.synthetic.main.layout_view_customer.*

/**
 * Created by asus on 11-Oct-18.
 */
class ViewUserActivity : SActivity() {
    lateinit var dataBaseHandler: DatabaseHandler
    lateinit var adapter: UserAdapter
    var userList = ArrayList<UserModel>()
    var scanType: String = ""
    var scanValue: String = ""

    lateinit var existingLocale: Locale
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_user_list)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.view_users_list_text_string)

        dataBaseHandler = DatabaseHandler(applicationContext)

        existingLocale = currentLanguage

        recyclerView.layoutManager = LinearLayoutManager(applicationContext, LinearLayout.VERTICAL, false)

        adapter = UserAdapter(userList, this,dataBaseHandler,currentLanguage, getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERNAME,""))
        val users: ArrayList<UserModel> = dataBaseHandler.getEmployeeList()!!
        if (users.size > 0) {
            if (users.size == 1) {
                users.forEach {

                    println(it)

                    if (it.KEY_EMP_FULLNMAE == "admin" && it.KEY_EMP_PWD == "admin") {
                        showMessageOK(resources.getString(R.string.user_empty_list_alert_text_string))
                        return@forEach
                    }
                }
            } else {
                users.forEach {

                    println(it)

                    if (it.KEY_EMP_FULLNMAE == "admin" && it.KEY_EMP_PWD == "admin") {
                        // showMessageOK(resources.getString(R.string.user_empty_list_alert_text_string))
                        return@forEach
                    } else {
                        userList.add(it)
                        adapter.notifyDataSetChanged()
                    }

                }
            }
            //now adding the adapter to recyclerview
            recyclerView.adapter = adapter

        } else {

            showMessageOK(getString(R.string.customer_empty_list_alert_text_string))
        }
        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(arg0: Editable) {
                // TODO Auto-generated method stub
                filter(arg0.toString())
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int,
                                           arg2: Int, arg3: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int,
                                       arg3: Int) {
                // TODO Auto-generated method stub


            }
        })


    }

    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdNames = ArrayList<UserModel>()

        //looping through existing elements
        for (userLists in userList) {
            //if the existing elements contains the search input

            if (userLists.KEY_EMP_EMAIL.toLowerCase().contains(text.toLowerCase()) ||
                    userLists.KEY_EMP_FULLNMAE.toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(userLists)
            }

        }

        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames)
    }

    private fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@ViewUserActivity)
        // Set the alert dialog title
        builder.setTitle(getString(R.string.confirmation_alert_text_string))
        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            onBackPressed()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }


}