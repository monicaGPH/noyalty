package com.gph.scanner

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.support.multidex.BuildConfig
import android.support.multidex.MultiDexApplication
import com.gph.scanner.service.ScannerFunctionService
import com.gph.scanner.service.ScannerFunctionServiceImpl
import com.gph.scanner.utils.AppConstant
import com.squareup.leakcanary.LeakCanary
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate


/**
 * Created by asus on 02-Aug-18.
 */
class ScannerApplication : MultiDexApplication() {

    private var appPreferences: SharedPreferences? = null
    private var scannerFunctionService: ScannerFunctionService? = null
    private var mInstance: ScannerApplication? = null
    private var status: String? = null
    var localizationDelegate = LocalizationApplicationDelegate(this)

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        mInstance = this

        //setupLeakCanary();
        setupLogLevel()
        setupMainSettings()

    }


    private fun setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }

        LeakCanary.install(this)
        // Normal app init code...
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base))
    }

    private fun setupLogLevel() {
        if (!isInDebugMode()!!) {

        }
        Logger.addLogAdapter(AndroidLogAdapter())
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localizationDelegate.onConfigurationChanged(this)
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }

    fun isInDebugMode(): Boolean? {
        return BuildConfig.DEBUG
    }


    private fun setupMainSettings() {
        appPreferences = getSharedPreferences(AppConstant.APP_PREF_FILE, Context.MODE_PRIVATE)
        scannerFunctionService = ScannerFunctionServiceImpl(this)
    }

    fun getAppPreferences(): SharedPreferences? {
        return appPreferences
    }


    fun getscannerFunctionService(): ScannerFunctionService? {
        return scannerFunctionService
    }


    @Synchronized
    fun getInstance(): ScannerApplication? {
        return mInstance
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }

    fun isOnline(value: Boolean) {
        if (value) {
            setStatus("Connected")
        } else {
            setStatus("DisConnected")
        }

    }


}