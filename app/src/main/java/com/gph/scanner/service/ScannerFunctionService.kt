package com.gph.scanner.service

import com.gph.scanner.utils.ForegroundTaskDelegate

/**
 * Created by asus on 02-Aug-18.
 */
interface ScannerFunctionService {
    abstract fun loadLoginRequirement(callback: ForegroundTaskDelegate<Int>)

    abstract fun logout(callback: ForegroundTaskDelegate<Boolean>)
}