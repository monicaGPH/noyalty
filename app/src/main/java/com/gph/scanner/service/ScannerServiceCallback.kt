package com.gph.scanner.service

import android.os.AsyncTask
import java.util.*

/**
 * Created by asus on 02-Aug-18.
 */
interface ScannerServiceCallback<Result : Any> {

    abstract fun setAsyncTask(task: AsyncTask<*, *, *>)

    abstract fun cancel()

    abstract fun onPreExecute()

    abstract fun onPostExecute(result: Result)
}