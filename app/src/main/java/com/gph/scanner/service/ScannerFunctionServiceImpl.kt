package com.gph.scanner.service

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.text.TextUtils
import com.gph.scanner.ScannerApplication
import com.gph.scanner.model.LoginRequirement
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.ForegroundTaskDelegate
import com.orhanobut.logger.Logger



/**
 * Created by asus on 02-Aug-18.
 */
class ScannerFunctionServiceImpl : ScannerFunctionService {
    override fun logout(callback: ForegroundTaskDelegate<Boolean>) {
        object : AsyncTask<Void, Void, Boolean>() {
            private val exception: Exception? = null

            override fun onPreExecute() {
                callback.setAsyncTask(this)
            //    callback.onPreExecute()
            }

            override fun doInBackground(vararg params: Void): Boolean? {

                Logger.d("Clear token and user info")
                appPreferences!!.edit().putString(AppConstant.PREF_KEY_USERNAME, "").apply()
                appPreferences!!.edit().putString(AppConstant.PREF_KEY_PASSWORD, "").apply()
                appPreferences!!.edit().putString(AppConstant.PREF_KEY_USERRIGHT, "").apply()
                return true
            }

            override fun onPostExecute(result: Boolean) {
                callback.onPostExecute(result)
            }
        }.execute()
    }


    @SuppressLint("StaticFieldLeak")
    override fun loadLoginRequirement(callback: ForegroundTaskDelegate<Int>) {

        object : AsyncTask<Void, Void, Int>() {
            private val exception: Exception? = null

            override fun onPreExecute() {
                callback!!.setAsyncTask(this)
                /*     callback.onPreExecute();*/
            }

            override fun doInBackground(vararg params: Void): Int? {
                try {
                    // Normally we would do some work here, like load data from server.
                    // For our sample, we just sleep for 3 seconds.

                } catch (exception: Exception) {
                    exception.message?.let { Logger.e(it) }

                    return LoginRequirement.LOGIN.ordinal
                }

                val username: String = getUserName()
                Logger.d("username " + username)

                return if (TextUtils.isEmpty(username)) {
                    LoginRequirement.LOGIN.ordinal
                } else {
                    LoginRequirement.NONE.ordinal
                }
            }

            override fun onPostExecute(result: Int) {
                callback.onPostExecute(result)
            }
        }.execute()
    }


    private lateinit var context: Context
    private var appPreferences: SharedPreferences? = null
    val timeOut: Long = 90 // 90 seconds

    constructor() {
        this.appPreferences = null
    }

    constructor(application: ScannerApplication) {
        this.context = application.applicationContext
        this.appPreferences = application.getAppPreferences()
    }


    private fun getUserName(): String {
        return appPreferences!!.getString(AppConstant.PREF_KEY_USERNAME, "")
    }

    private fun getUserPwd(): String {
        return appPreferences!!.getString(AppConstant.PREF_KEY_PASSWORD, "")
    }

    private fun getUserRight(): String {
        return appPreferences!!.getString(AppConstant.PREF_KEY_USERRIGHT, "")
    }


}