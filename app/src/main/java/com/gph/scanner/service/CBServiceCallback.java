package com.gph.scanner.service;

import android.os.AsyncTask;


/**
 * Created by Inverse, LLC on 10/18/16.
 */

public interface CBServiceCallback<Result extends Object> {

    void setAsyncTask(AsyncTask task);

    void cancel();

    void onPreExecute();

    void onPostExecute(Result result);

}

