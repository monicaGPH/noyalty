package com.gph.scanner.utils.database


import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException

import android.database.sqlite.SQLiteOpenHelper
import com.orhanobut.logger.Logger
import java.sql.SQLException
import com.gph.scanner.model.*


/**
 * Created by asus on 31-Jul-18.
 */

class DatabaseHandler(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME,
        null, DATABASE_VERSION) {

    // All Static variables
    // Database Version
    companion object {
        private val DATABASE_VERSION = 5

        // Database Name
        private val DATABASE_NAME = "noyalty.db"

        // storetable table name
        private val TABLE_STORE = "storetable"

        // storetable Table Columns names
        private val KEY_STORE_ID = "storeid"
        private val KEY_STORENAME = "storename"
        private val KEY_STORE_ADDRESS = "storeadd"
        private val KEY_STORE_EMAIL_ADDRESS = "storeemail"
        private val KEY_MEMBERSHIPID = "membershipid"


        // employeetable table name
        private val TABLE_EMPLOYEE = "employeetable"

        // employeetable Table Columns names
        private val KEY_EMP_ID = "empid"
        private val KEY_EMP_FULLNMAE = "fullname"
        private val KEY_EMP_EMAIL = "email"
        private val KEY_EMP_PWD = "pwd"
        private val KEY_USER_RIGHT = "role"

        // vouchertable table name
        private val TABLE_VOUCHER = "vouchertable"

        // employeetable Table Columns names
        private val KEY_VOUCHER_GUEST_ID = "guestid"
        private val KEY_VOUCHER_VALUE = "voucher"
        private val KEY_VOUCHER_CREATED_AT = "created"


        // guesttable table name
        private val TABLE_GUEST = "guesttable"

        // guesttable Table Columns names
        private val KEY_GUEST_ID = "guestid"
        private val KEY_GUEST_NAME = "guestname"
        private val KEY_GUSET_ADD = "guestadd"
        private val KEY_GUEST_MOBILE_NUMBER = "mobileno"
        private val KEY_GUEST_EMAIL = "guestemail"
        private val KEY_GUEST_PROFILE_PICTURE = "guestpicture"
        private val KEY_MEMBERSHIPID_ID = "membershipid"
        private val KEY_NFC_ID = "nfcid"
        private val KEY_QR_CODE_ID = "qrcodeid"
        private val KEY_CREATED_AT = "created_at"

        // guestactivitytable table name
        private val TABLE_GUEST_ACTIVITY = "guestactivitytable"

        // guestactivitytable Table Columns names
        private val KEY_GUEST_ACTIVITY_ID = "guestactid"
        private val KEY_GUSET_ACT_STOREID = "guestactstoreid"
        private val KEY_GUEST_ACT_CREATEDAT = "ceratedat"
        private val KEY_GUEST_ACT_GUESTID = "guestactguestid"
        private val KEY_GUEST_ACT_TRANSACTIONID = "transactionid"
        private val KEY_LIFETIME_STAMP = "lifetimestamp"
        private val KEY_CREDIT = "credit"

        // guestTransactionatable table name
        private val TABLE_GUEST_TRANSACTION_ACTIVITY = "guesttransactiontable"

        // guestTransactionatable Table Columns names
        private val KEY_GUEST_TRANSACTION_ACTIVITY_ID = "guesttransid"
        private val KEY_GUEST_TRANSACTION_ID = "guesttransidforeign"
        private val KEY_GUEST_TRANSACTION_REDEEM = "redeem"
        private val KEY_GUEST_TRANSACTION_REWARD = "reward"
        private val KEY_GUEST_TRANSACTION_STAMP = "stamp"
        private val KEY_GUEST_TRANSACTION_UPDATED_BY = "updatedby"
        private val KEY_GUEST_TRANSACTION_UPDATED = "updatedat"


    }

    // Creating Tables
    override fun onCreate(db: SQLiteDatabase) {


        val CREATE_GUEST_TABLE = ("CREATE TABLE " + TABLE_GUEST + "("
                + KEY_GUEST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_GUEST_NAME + " TEXT ,"
                + KEY_GUSET_ADD + " TEXT ,"
                + KEY_GUEST_MOBILE_NUMBER + " TEXT , "
                + KEY_GUEST_EMAIL + " TEXT , "
                + KEY_MEMBERSHIPID_ID + " TEXT , "
                + KEY_NFC_ID + " TEXT , "
                + KEY_QR_CODE_ID + " TEXT , "
                + KEY_GUEST_PROFILE_PICTURE + " TEXT , "
                + KEY_CREATED_AT + " TEXT"
                + ")")

        val CREATE_EMPLOYEE_TABLE = ("CREATE TABLE " + TABLE_EMPLOYEE + "("
                + KEY_EMP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_EMP_FULLNMAE + " TEXT ,"
                + KEY_EMP_EMAIL + " TEXT ,"
                + KEY_EMP_PWD + " TEXT , "
                + KEY_USER_RIGHT + " TEXT"
                + ")")

        val CREATE_STORE_TABLE = ("CREATE TABLE " + TABLE_STORE + "("
                + KEY_STORE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_STORENAME + " TEXT ,"
                + KEY_STORE_ADDRESS + " TEXT ,"
                + KEY_STORE_EMAIL_ADDRESS + " TEXT ,"
                + KEY_MEMBERSHIPID + " TEXT"
                + ")")

        val CREATE_GUEST_ACTIVITY_TABLE = ("CREATE TABLE " + TABLE_GUEST_ACTIVITY + "("
                + KEY_GUEST_ACTIVITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_GUSET_ACT_STOREID + " TEXT ,"
                + KEY_GUEST_ACT_CREATEDAT + " TEXT ,"
                + KEY_GUEST_ACT_GUESTID + " TEXT ,"
                + KEY_GUEST_ACT_TRANSACTIONID + " TEXT ,"
                + KEY_LIFETIME_STAMP + " TEXT ,"
                + KEY_CREDIT + " TEXT"
                + ")")
        val CREATE_GUEST_TRANSACTION_TABLE = ("CREATE TABLE " + TABLE_GUEST_TRANSACTION_ACTIVITY + "("
                + KEY_GUEST_TRANSACTION_ACTIVITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_GUEST_TRANSACTION_ID + " INTEGER ,"
                + KEY_GUEST_TRANSACTION_REDEEM + " TEXT ,"
                + KEY_GUEST_TRANSACTION_REWARD + " INTEGER ,"
                + KEY_GUEST_TRANSACTION_STAMP + " INTEGER ,"
                + KEY_GUEST_TRANSACTION_UPDATED_BY + " TEXT ,"
                + KEY_GUEST_TRANSACTION_UPDATED + " TEXT"
                + ")")

        val CREATE_GUEST_VOUCHER_TABLE = ("CREATE TABLE " + TABLE_VOUCHER + "("
                + KEY_VOUCHER_GUEST_ID + " INTEGER ,"
                + KEY_VOUCHER_VALUE + " TEXT ,"
                + KEY_VOUCHER_CREATED_AT + " TEXT"
                + ")")
        db.execSQL(CREATE_EMPLOYEE_TABLE)
        db.execSQL(CREATE_GUEST_TABLE)
        db.execSQL(CREATE_STORE_TABLE)
        db.execSQL(CREATE_GUEST_ACTIVITY_TABLE)
        db.execSQL(CREATE_GUEST_TRANSACTION_TABLE)
        db.execSQL(CREATE_GUEST_VOUCHER_TABLE)

        this.addEmployee(com.gph.scanner.model.Account("admin", "", "admin", "admin"), db)

    }

    // Upgrading database
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Drop older table if existed
        if (oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS $TABLE_GUEST")
            db.execSQL("DROP TABLE IF EXISTS $TABLE_EMPLOYEE")
            db.execSQL("DROP TABLE IF EXISTS $TABLE_STORE")
            db.execSQL("DROP TABLE IF EXISTS $TABLE_GUEST_ACTIVITY")
            db.execSQL("DROP TABLE IF EXISTS $TABLE_GUEST_TRANSACTION_ACTIVITY")
            db.execSQL("DROP TABLE IF EXISTS $TABLE_VOUCHER")
            // Create tables again
            onCreate(db)
        }
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */


    // Add employee information
    fun addEmployee(account: Account, db: SQLiteDatabase) {

        try {
            val values = ContentValues()
            values.put(KEY_EMP_FULLNMAE, account.emp_fullname)
            values.put(KEY_EMP_EMAIL, account.emp_email)
            values.put(KEY_EMP_PWD, account.emp_pwd)
            values.put(KEY_USER_RIGHT, account.emp_right)
            // Inserting Row
            db.insert(TABLE_EMPLOYEE, null, values)

        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }

    // Add employee information
    fun addEmployee(account: Account) {
        val db: SQLiteDatabase = this.writableDatabase
        try {

            val values = ContentValues()
            values.put(KEY_EMP_FULLNMAE, account.emp_fullname)
            values.put(KEY_EMP_EMAIL, account.emp_email)
            values.put(KEY_EMP_PWD, account.emp_pwd)
            values.put(KEY_USER_RIGHT, account.emp_right)
            // Inserting Row
            db.insert(TABLE_EMPLOYEE, null, values)

        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }

    // Add guest information
    fun addGuest(guest: Guest) {
        try {

            val db: SQLiteDatabase = this.writableDatabase
            val values = ContentValues()
            values.put(KEY_GUEST_NAME, guest.guest_name)
            values.put(KEY_GUSET_ADD, guest.guest_add)
            values.put(KEY_GUEST_MOBILE_NUMBER, guest.guest_mobile_number)
            values.put(KEY_GUEST_EMAIL, guest.guest_email)
            values.put(KEY_MEMBERSHIPID_ID, guest.guest_membershipId)
            values.put(KEY_NFC_ID, guest.guest_nfc_id)
            values.put(KEY_QR_CODE_ID, guest.guest_card_id)
            values.put(KEY_GUEST_PROFILE_PICTURE, guest.guest_profile_picture)
            values.put(KEY_CREATED_AT, guest.guest_created_at)
            // Inserting Row
            db.insert(TABLE_GUEST, null, values)

        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }


    // Add guest transaction information
    fun addGuestTransaction(guest: GuestTransaction) {
        try {

            val db: SQLiteDatabase = this.writableDatabase
            val values = ContentValues()

            values.put(KEY_GUEST_TRANSACTION_ID, guest.KEY_GUEST_TRANSACTION_ID)
            values.put(KEY_GUEST_TRANSACTION_REDEEM, guest.KEY_GUEST_TRANSACTION_REDEEM)
            values.put(KEY_GUEST_TRANSACTION_REWARD, guest.KEY_GUEST_TRANSACTION_REWARD)
            values.put(KEY_GUEST_TRANSACTION_STAMP, guest.KEY_GUEST_TRANSACTION_STAMP)
            values.put(KEY_GUEST_TRANSACTION_UPDATED_BY, guest.KEY_GUEST_TRANSACTION_UPDATEDBY)
            values.put(KEY_GUEST_TRANSACTION_UPDATED, guest.KEY_GUEST_TRANSACTION_UPDATED)

            // Inserting Row
            db.insert(TABLE_GUEST_TRANSACTION_ACTIVITY, null, values)

        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }

    // Add store information
    fun addStore(store: Store) {
        try {

            val db: SQLiteDatabase = this.writableDatabase
            val values = ContentValues()
            values.put(KEY_STORENAME, store.store_name)
            values.put(KEY_STORE_ADDRESS, store.store_address)
            values.put(KEY_MEMBERSHIPID, store.membership_id)
            values.put(KEY_STORE_EMAIL_ADDRESS, store.store_email_address)
            // Inserting Row
            db.insert(TABLE_STORE, null, values)
        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }


    // Add guestActivity information
    fun addGuestActivity(guestActivity: GuestActivity) {
        try {

            val db: SQLiteDatabase = this.writableDatabase
            val values = ContentValues()
            values.put(KEY_GUSET_ACT_STOREID, guestActivity.activity_store_id)
            values.put(KEY_GUEST_ACT_CREATEDAT, guestActivity.activity_created_at)
            values.put(KEY_GUEST_ACT_GUESTID, guestActivity.activity_guest_id)
            values.put(KEY_GUEST_ACT_TRANSACTIONID, guestActivity.activity_transactionid)
            values.put(KEY_LIFETIME_STAMP, guestActivity.lifetime_stamp)
            values.put(KEY_CREDIT, guestActivity.credit)

            // Inserting Row
            db.insert(TABLE_GUEST_ACTIVITY, null, values)
        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }

    // Add voucher information
    fun addVoucher(voucher: Voucher) {
        val db: SQLiteDatabase = this.writableDatabase
        try {

            val values = ContentValues()
            values.put(KEY_VOUCHER_GUEST_ID, voucher.guest_voucher_id)
            values.put(KEY_VOUCHER_VALUE, voucher.voucher_value)
            values.put(KEY_VOUCHER_CREATED_AT, voucher.voucher_created_at)
            // Inserting Row
            db.insert(TABLE_VOUCHER, null, values)

        } catch (ex: SQLException) {
            ex.printStackTrace()
        }

    }
    // check guest info by QR/BarCode exist or not

    fun checkGuestEmployeeExistorNotCard(codenumber: String?): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val cursor: Cursor?
        val selectQuery = "SELECT * FROM " + TABLE_GUEST + " WHERE " + KEY_QR_CODE_ID + " = '" + codenumber?.trim({ it <= ' ' }) + "'"

        cursor = db.rawQuery(selectQuery, null)
        Logger.d("Cursor Count : " + cursor!!.count)

        return if (cursor.count > 0) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }

    }

    // check guest info by NFC exist or not

    fun checkGuestEmployeeExistorNotNFC(NFCnumber: String?): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val cursor: Cursor?
        val selectQuery = "SELECT * FROM " + TABLE_GUEST + " WHERE " + KEY_NFC_ID + " = '" + NFCnumber?.trim({ it <= ' ' }) + "'"

        cursor = db.rawQuery(selectQuery, null)
        Logger.d("Cursor Count : " + cursor!!.count)

        return if (cursor.count > 0) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }

    }

    // check store exist or not

    fun checkStoreExistorNot(): Boolean {
        try {
            val db: SQLiteDatabase = this.writableDatabase
            val cursor: Cursor?
            val selectQuery = "SELECT * FROM " + TABLE_STORE

            cursor = db.rawQuery(selectQuery, null)
            Logger.d("Cursor Count : " + cursor!!.count)

            return if (cursor.count > 0) {
                cursor.close()
                db.close()
                true
            } else {
                cursor.close()
                db.close()
                false
            }
        } catch (ex: SQLiteException) {
            ex.printStackTrace()
            return false
        }

    }
    // check employee exist or not

    fun checkEmployeeExistorNot(username: String?, pwd: String?): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val cursor: Cursor?
        // val selectQuery = "SELECT * FROM " + TABLE_EMPLOYEE + " WHERE " + KEY_EMP_FULLNMAE + " = '" + username?.trim({ it <= ' ' }) + "' AND " + KEY_EMP_PWD + " = '" + pwd?.trim({ it <= ' ' }) + "'"
        val selectQuery = "SELECT * FROM $TABLE_EMPLOYEE WHERE ($KEY_EMP_FULLNMAE = '$username' OR $KEY_EMP_EMAIL = '$username') AND $KEY_EMP_PWD = '$pwd'"
        Logger.d(selectQuery)
        cursor = db.rawQuery(selectQuery, null)
        Logger.d("Cursor Count : " + cursor!!.count)

        return if (cursor.count > 0) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }

    }
    // check voucher information exist or not

    fun checkVoucherInformationExistorNot(guest_id: String?): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val cursor: Cursor?

        val selectQuery = "SELECT * FROM " + TABLE_VOUCHER + " WHERE " + KEY_VOUCHER_GUEST_ID + " = '" + guest_id?.trim({ it <= ' ' }) + "'"
        Logger.d(selectQuery)
        cursor = db.rawQuery(selectQuery, null)
        Logger.d("Cursor Count : " + cursor!!.count)

        return if (cursor.count > 0) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }

    }
    // check voucher information exist or not

    fun checkGuestTransactionInformationExistorNot(guest_id: String?): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val cursor: Cursor?

        val selectQuery = "SELECT * FROM " + TABLE_GUEST_TRANSACTION_ACTIVITY + " WHERE " + KEY_GUEST_TRANSACTION_ID + " = '" + guest_id?.trim({ it <= ' ' }) + "'"
        Logger.d(selectQuery)
        cursor = db.rawQuery(selectQuery, null)
        Logger.d("Cursor Count : " + cursor!!.count)

        return if (cursor.count > 0) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }

    }

    @SuppressLint("Recycle")
    // get guest info by NFC
    fun getGuestBYnfcId(nfcid: String?): Guest? {
        var guest: Guest? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM " + TABLE_GUEST + " WHERE " + KEY_NFC_ID + " = '" + nfcid?.trim({ it <= ' ' }) + "' "
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                guest = Guest(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9))

            } while (cursor.moveToNext())

        }
        return guest
    }

    @SuppressLint("Recycle")
    // get guest info by guest_id
    fun getGuestInfoBYGuestID(guest_id: Int?): Guest? {
        var guest: Guest? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_GUEST  WHERE  $KEY_GUEST_ID = '$guest_id' "
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                guest = Guest(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9))
                Logger.d(cursor.getString(1))
                Logger.d(cursor.getString(2))
                Logger.d(cursor.getString(3))
                Logger.d(cursor.getString(4))
                Logger.d(cursor.getString(5))
                Logger.d(cursor.getString(6))
                Logger.d(cursor.getString(7))
                Logger.d(cursor.getString(8))
                Logger.d(cursor.getString(9))


            } while (cursor.moveToNext())

        }
        return guest
    }

    // get total redeem, reward,stamp
    data class GuestIDANDGuestCardID(val guest_id: Int,
                                     val guest_membership_id: String,
                                     val guest_card_id: String)

    @SuppressLint("Recycle")
    // get guest info by QR/BarCode
    fun getGuestIDBYnfcId(nfcid: String?): GuestIDANDGuestCardID {
        var guest_id: Int? = null
        var guest_card_id = ""
        var guest_membership_id = ""
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM " + TABLE_GUEST + " WHERE " + KEY_NFC_ID + " = '" + nfcid?.trim({ it <= ' ' }) + "' "
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                guest_id = cursor.getInt(0)
                guest_membership_id = cursor.getString(5)
                guest_card_id = cursor.getString(6)

            } while (cursor.moveToNext())

        }
        return GuestIDANDGuestCardID(guest_id!!, guest_membership_id, guest_card_id)
    }

    @SuppressLint("Recycle")
    // get guestID by QR/BarCode Information
    fun getGuestIDBYCARDId(cardid: String?): GuestIDANDGuestCardID {
        try {
            var guest_id: Int? = null
            var guest_card_id = ""
            var guest_membership_id = ""
            val db: SQLiteDatabase = this.writableDatabase

            val selectQuery = "SELECT * FROM " + TABLE_GUEST + " WHERE " + KEY_QR_CODE_ID + " = '" + cardid?.trim({ it <= ' ' }) + "' "
            val cursor = db.rawQuery(selectQuery, null)

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    guest_id = cursor.getInt(0)
                    guest_membership_id = cursor.getString(5)
                    guest_card_id = cursor.getString(7)


                } while (cursor.moveToNext())

            }
            return GuestIDANDGuestCardID(guest_id!!, guest_membership_id, guest_card_id)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
            return GuestIDANDGuestCardID(0, "", "")
        }
    }

    @SuppressLint("Recycle")
    // get guestID by NFC Information
    fun getGuestBYcardId(cardid: String?): Guest? {
        var guest: Guest? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM " + TABLE_GUEST + " WHERE " + KEY_QR_CODE_ID + " = '" + cardid?.trim({ it <= ' ' }) + "' "
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                guest = Guest(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9))

            } while (cursor.moveToNext())

        }
        return guest
    }

    @SuppressLint("Recycle")
    // get employee Information
    fun getEmployee(username: String?, passwrd: String?): Account? {
        var account: Account? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_EMPLOYEE WHERE ($KEY_EMP_FULLNMAE = '$username' OR $KEY_EMP_EMAIL = '$username') AND $KEY_EMP_PWD = '$passwrd'"
        Logger.e(selectQuery)
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Logger.e(cursor.getString(1))
                Logger.e(cursor.getString(2))
                Logger.e(cursor.getString(3))
                Logger.e(cursor.getString(4))
                account = Account(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4))

            } while (cursor.moveToNext())

        }
        return account
    }

    @SuppressLint("Recycle")
    // get employee Information
    fun getEmployeeList(): ArrayList<UserModel>? {

        val userModels: ArrayList<UserModel>? = ArrayList()
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_EMPLOYEE"
        Logger.e(selectQuery)
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                var userModel = UserModel(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4))
                userModels!!.add(userModel)
            } while (cursor.moveToNext())

        }
        return userModels
    }

    @SuppressLint("Recycle")
    // get Store information
    fun getStore(): Store? {

        var store: Store? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_STORE"
        @SuppressLint("Recycle") val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                store = Store(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4))

            } while (cursor.moveToNext())

        }
        return store
    }

    @SuppressLint("Recycle")
    // get Store information
    fun getVoucherInformation(guest_id: String): Voucher? {

        var voucher: Voucher? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM " + TABLE_VOUCHER + " WHERE " + KEY_VOUCHER_GUEST_ID + " = '" + guest_id?.trim({ it <= ' ' }) + "'"
        @SuppressLint("Recycle") val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                voucher = Voucher(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2))

            } while (cursor.moveToNext())

        }
        return voucher
    }

    // check GuestActivity information by guestID exist or not

    fun checkGuestActivityExistorNot(guest_id: Int?): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val cursor: Cursor?
        val selectQuery = "SELECT * FROM $TABLE_GUEST_ACTIVITY WHERE $KEY_GUEST_ACT_GUESTID = '$guest_id'"

        cursor = db.rawQuery(selectQuery, null)
        Logger.d("Cursor Count : " + cursor!!.count)

        return if (cursor.count > 0) {
            cursor.close()
            db.close()
            true
        } else {
            cursor.close()
            db.close()
            false
        }

    }

    @SuppressLint("Recycle")
    // get GuestActivity info by GuestID
    fun getGuestActivityeByGuestID(guest_id: Int): GuestActivity? {

        var guestActivity: GuestActivity? = null
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_GUEST_ACTIVITY WHERE $KEY_GUEST_ACT_GUESTID = '$guest_id'"
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                guestActivity = GuestActivity(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6))

            } while (cursor.moveToNext())

        }
        return guestActivity
    }

    @SuppressLint("Recycle")
    // get all Guest info in arrylist
    fun getGuest(): ArrayList<Guest>? {

        val guests: ArrayList<Guest>? = ArrayList()
        val db: SQLiteDatabase = this.writableDatabase

        val selectQuery = "SELECT * FROM " + TABLE_GUEST
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                val guest = Guest(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9))

                guests!!.add(guest)
            } while (cursor.moveToNext())
        }
        return guests
    }

    @SuppressLint("Recycle")
    // get all guest transaction info in arraylist
    fun getGuestTransaction(guest_id: Int): ArrayList<GuestTransaction>? {

        val guestTransactions: ArrayList<GuestTransaction>? = ArrayList()

        val db: SQLiteDatabase = this.writableDatabase
        try {
            val selectQuery = "SELECT * FROM $TABLE_GUEST_TRANSACTION_ACTIVITY WHERE $KEY_GUEST_TRANSACTION_ID = '$guest_id'"
            Logger.d(selectQuery)
            val cursor = db.rawQuery(selectQuery, null)
            Logger.d(cursor.toString())
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    var guestTransaction: GuestTransaction?
                    guestTransaction = GuestTransaction(
                            cursor.getString(0),
                            cursor.getString(1).toInt(),
                            cursor.getString(2),
                            cursor.getInt(3),
                            cursor.getInt(4),
                            cursor.getString(5),
                            cursor.getString(6))
                    guestTransactions!!.add(guestTransaction)
                } while (cursor.moveToNext())

            }
        } catch (ex: SQLiteException) {
            ex.printStackTrace()
        }
        return guestTransactions
    }

    @SuppressLint("Recycle")
    // get all guest transaction info
    fun getGuestTransactionBYGuestID(guest_id: Int): GuestTransaction? {
        var guestTransaction: GuestTransaction? = null
        val db: SQLiteDatabase = this.writableDatabase
        try {
            val selectQuery = "SELECT * FROM $TABLE_GUEST_TRANSACTION_ACTIVITY WHERE $KEY_GUEST_TRANSACTION_ID = '$guest_id' ORDER BY $KEY_GUEST_TRANSACTION_ACTIVITY_ID DESC LIMIT 1"
            Logger.d(selectQuery)
            val cursor = db.rawQuery(selectQuery, null)

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    guestTransaction = GuestTransaction(
                            cursor.getString(0),
                            cursor.getString(1).toInt(),
                            cursor.getString(2),
                            cursor.getInt(3),
                            cursor.getInt(4),
                            cursor.getString(5),
                            cursor.getString(6))

                } while (cursor.moveToNext())

            }
        } catch (ex: SQLiteException) {
            ex.printStackTrace()
        }
        return guestTransaction
    }

    // get total redeem, reward,stamp
    data class Total(val redeem: Int,
                     val reward: Int,
                     val stamp: Int)

    @SuppressLint("Recycle")
    fun getGuestTransactionTotal(guest_id: Int): Total {
        val db: SQLiteDatabase = this.writableDatabase
        var total_redeem = 0
        var total_reward = 0
        var total_stamp = 0
        try {
            val selectQuery = "select total($KEY_GUEST_TRANSACTION_REDEEM),total($KEY_GUEST_TRANSACTION_REWARD),total($KEY_GUEST_TRANSACTION_STAMP) from $TABLE_GUEST_TRANSACTION_ACTIVITY WHERE $KEY_GUEST_TRANSACTION_ID = '$guest_id'"
            val cursor = db.rawQuery(selectQuery, null)

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    total_redeem = cursor.getString(0).toInt()
                    total_reward = cursor.getString(1).toInt()
                    total_stamp = cursor.getString(2).toInt()
                    Logger.d(total_redeem)
                    Logger.d(total_reward)
                    Logger.d(total_stamp)
                } while (cursor.moveToNext())
            }
        } catch (ex: SQLiteException) {
            ex.printStackTrace()

        }
        return Total(total_redeem, total_reward, total_stamp)
    }

    // Updating Guestactivity table
    fun updateGuestActivity(guestActivity: GuestActivity, guest_id: String) {

        try {
            val db = this.writableDatabase


            val values = ContentValues()
            values.put(KEY_GUEST_ACT_CREATEDAT, guestActivity.activity_created_at)
            values.put(KEY_GUEST_ACT_TRANSACTIONID, guestActivity.activity_transactionid)
            values.put(KEY_LIFETIME_STAMP, guestActivity.lifetime_stamp)
            values.put(KEY_CREDIT, guestActivity.credit)
            // updating row
            db.update(TABLE_GUEST_ACTIVITY,
                    values,
                    KEY_GUEST_ACT_GUESTID + " = ?",
                    arrayOf(guest_id))


        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()

        }

    }

    // Updating Guest byt NFC/BarCode tag table
    fun updateGuest(guest: Guest, id: String, type: String) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()
            if (type.equals("NFC", true)) {
                values.put(KEY_GUEST_NAME, guest.guest_name)
                values.put(KEY_GUSET_ADD, guest.guest_add)
                values.put(KEY_GUEST_MOBILE_NUMBER, guest.guest_mobile_number)
                values.put(KEY_GUEST_EMAIL, guest.guest_email)
                values.put(KEY_GUEST_PROFILE_PICTURE, guest.guest_profile_picture)
                values.put(KEY_MEMBERSHIPID_ID, guest.guest_membershipId)

                // updating row
                db.update(TABLE_GUEST, values, KEY_NFC_ID + " = ?",
                        arrayOf(id.trim { it <= ' ' }))
            } else if (type.equals("CARD", true)) {
                values.put(KEY_GUEST_NAME, guest.guest_name)
                values.put(KEY_GUSET_ADD, guest.guest_add)
                values.put(KEY_GUEST_MOBILE_NUMBER, guest.guest_mobile_number)
                values.put(KEY_GUEST_EMAIL, guest.guest_email)
                values.put(KEY_GUEST_PROFILE_PICTURE, guest.guest_profile_picture)
                values.put(KEY_MEMBERSHIPID_ID, guest.guest_membershipId)

                // updating row
                db.update(TABLE_GUEST, values, KEY_QR_CODE_ID + " = ?",
                        arrayOf(id.trim { it <= ' ' }))
            }
        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()

        }

    }

    // Updating Guest byt NFC/BarCode tag table
    fun updateGuest(guest: Guest, guest_id: Int) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()

            values.put(KEY_GUEST_NAME, guest.guest_name)
            values.put(KEY_GUSET_ADD, guest.guest_add)
            values.put(KEY_GUEST_MOBILE_NUMBER, guest.guest_mobile_number)
            values.put(KEY_GUEST_PROFILE_PICTURE, guest.guest_profile_picture)
            values.put(KEY_GUEST_EMAIL, guest.guest_email)
            values.put(KEY_CREATED_AT, guest.guest_created_at)
            // updating row
            db.update(TABLE_GUEST, values, KEY_GUEST_ID + " = " + guest_id, null)

        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()

        }

    }

    // update last visit date
    fun updateGuestLastVisitDate(guest: Guest, guest_id: Int) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()
            values.put(KEY_CREATED_AT, guest.guest_created_at)
            // updating row
            db.update(TABLE_GUEST, values, KEY_GUEST_ID + " = " + guest_id, null)

        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()

        }

    }

    // update employee password date
    fun updateEmployeePwd(pwd: String, username: String) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()

            values.put(KEY_EMP_PWD, pwd)
            // updating row
            db.update(TABLE_EMPLOYEE, values, "$KEY_EMP_FULLNMAE = $username OR $KEY_EMP_EMAIL = $username", null)

        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()

        }

    }


    // Updating store table
    fun updateStore(store: Store, store_id: String) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()

            values.put(KEY_STORENAME, store.store_name)
            values.put(KEY_STORE_ADDRESS, store.store_address)
            values.put(KEY_STORE_EMAIL_ADDRESS, store.store_email_address)

            // updating row
            db.update(TABLE_STORE, values, KEY_STORE_ID + " = ?",
                    arrayOf(store_id))
        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()
        }

    }

    // Updating voucher table
    fun updateVoucher(voucher: Voucher, guest_id: String) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()

            values.put(KEY_VOUCHER_VALUE, voucher.voucher_value)
            values.put(KEY_VOUCHER_CREATED_AT, voucher.voucher_created_at)

            // updating row
            db.update(TABLE_VOUCHER, values, "$KEY_VOUCHER_GUEST_ID = $guest_id", null)
        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()
        }

    }

    // Updating voucher table
    fun updateEmployee(userModel: UserModel, user_id: Int) {

        try {
            val db = this.writableDatabase
            val values = ContentValues()

            values.put(KEY_EMP_FULLNMAE, userModel.KEY_EMP_FULLNMAE)
            values.put(KEY_EMP_EMAIL, userModel.KEY_EMP_EMAIL)

            // updating row
            db.update(TABLE_EMPLOYEE, values, "$KEY_EMP_ID = $user_id", null)
        } catch (ex: android.database.SQLException) {
            ex.printStackTrace()
        }

    }

    // Deleting guest info by NFC/QRor BARCODE Code
    fun deleteGuestData(scanValue: String, scanType: String) {
        val db = this.writableDatabase
        if (scanType.equals("NFC", true)) {
            db.delete(TABLE_GUEST, KEY_NFC_ID + "=\"" + scanValue.trim({ it <= ' ' }) + "\"", null)
            // db.execSQL("delete from " + TABLE_GUEST + "  WHERE  " + KEY_NFC_ID + " = " + scanValue.trim({ it <= ' ' }))
            db.close()
        } else if (scanType.equals("CARD", true)) {

            db.delete(TABLE_GUEST, KEY_QR_CODE_ID + "=\"" + scanValue.trim({ it <= ' ' }) + "\"", null)
            //  db.execSQL("delete from " + TABLE_GUEST + "  WHERE  " + KEY_QR_CODE_ID + " = " + scanValue.trim({ it <= ' ' }))
            db.close()
        }
    }

    // Deleting guest info by NFC/QRor BARCODE Code
    fun deleteGuestCardId(scanValue: String, scanType: String) {
        if (scanType.equals("NFC", true)) {

            try {
                val db = this.writableDatabase
                val values = ContentValues()
                values.put(KEY_NFC_ID, "")
                // updating row
                db.update(TABLE_GUEST, values, KEY_NFC_ID + "=\"" + scanValue + "\"", null)
                // db.update(TABLE_GUEST, values, "$KEY_NFC_ID = '$scanValue'", null)
                db.close()
            } catch (ex: android.database.SQLException) {
                ex.printStackTrace()
            }

        } else if (scanType.equals("CARD", true)) {

            try {
                val db = this.writableDatabase
                val values = ContentValues()
                values.put(KEY_QR_CODE_ID, "")
                // updating row
                db.update(TABLE_GUEST, values, KEY_QR_CODE_ID + "=\"" + scanValue + "\"", null)
                //  db.update(TABLE_GUEST, values, "$KEY_QR_CODE_ID = '$scanValue'", null)
                db.close()
            } catch (ex: android.database.SQLException) {
                ex.printStackTrace()
            }

        }

    }

    @SuppressLint("Recycle")
// Deleting guest info by NFC/QRor BARCODE Code
    fun deleteGuestQRCodeData(scanValue: String) {
        val db = this.writableDatabase

        db.execSQL("DELETE FROM $TABLE_GUEST WHERE $KEY_QR_CODE_ID = '$scanValue'")
        db.close()

    }

    @SuppressLint("Recycle")
// Deleting GuestActivity data according to guest_id
    fun deleteGuestActivityData(guest_id: String) {
        val db = this.writableDatabase

        db.execSQL("DELETE FROM $TABLE_GUEST_ACTIVITY WHERE $KEY_GUEST_ACT_GUESTID = '$guest_id'")
        db.close()

    }

    @SuppressLint("Recycle")
// Deleting GuestTransactionActivity data according to guest_id
    fun deleteGuestTransactionActivityData(guest_id: String) {
        val db = this.writableDatabase

        db.execSQL("DELETE FROM $TABLE_GUEST_TRANSACTION_ACTIVITY WHERE $KEY_GUEST_TRANSACTION_ID = '$guest_id'")
        db.close()

    }

    @SuppressLint("Recycle")
// Deleting guest info by NFC/QRor BARCODE Code
    fun deleteEmployee(user_id: String) {
        val db = this.writableDatabase

        db.execSQL("DELETE FROM $TABLE_EMPLOYEE WHERE $KEY_EMP_ID = '$user_id'")
        db.close()

    }

    @SuppressLint("Recycle")
// Deleting guest info by NFC/QRor BARCODE Code
    fun deleteGuestNFCData(scanValue: String) {
        val db = this.writableDatabase

        db.execSQL("DELETE FROM $TABLE_GUEST WHERE $KEY_NFC_ID = '$scanValue'")
        db.close()

    }

}