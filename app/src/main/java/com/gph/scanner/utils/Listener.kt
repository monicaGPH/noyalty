package com.gph.scanner.utils

/**
 * Created by asus on 07-Aug-18.
 */
interface Listener {

    fun onDialogDisplayed()

    fun onDialogDismissed()
}