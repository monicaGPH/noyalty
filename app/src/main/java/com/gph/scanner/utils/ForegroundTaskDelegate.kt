package com.gph.scanner.utils

import android.os.AsyncTask

import com.gph.scanner.activity.SActivity
import com.gph.scanner.service.ScannerServiceCallback
import java.lang.ref.WeakReference


/**
 * Created by asus on 03-Aug-18.
 */
open class ForegroundTaskDelegate<Result : Any> : ScannerServiceCallback<Result> {


     var activityWeakReference: WeakReference<SActivity>
    var task: AsyncTask<*, *, *>? = null



    constructor(activity: SActivity){
        activityWeakReference = WeakReference(activity)
    }


    override fun setAsyncTask(task: AsyncTask<*, *, *>) {
        cancelAsyncTask()
        this.task = task
    }



    override fun cancel() {
        cancelAsyncTask()

    }

    protected fun cancelAsyncTask() {
        if (task != null && !task!!.isCancelled) {
            task!!.cancel(true)
        }
    }



    protected fun showProgress() {
        val activity = activityWeakReference.get()
        if (activity != null && !activity!!.isFinishing) {
            activity!!.showProgressDialog()
        }
    }

    protected fun dismissProgress() {
        val activity = activityWeakReference.get()
        if (activity != null && !activity!!.isFinishing) {
            activity!!.dismissProgressDialog()
        }
    }


  override  fun onPreExecute() {
        showProgress()
    }

    override fun onPostExecute(result: Result) {
        dismissProgress()

        if (result == null) {
        }


    }
}
