package com.gph.scanner.utils

/**
 * Created by asus on 02-Aug-18.
 */
class AppConstant {
    companion object {
        val APP_PREF_FILE: String = "scanner_Prefs.xml"
        val PREF_KEY_USERNAME: String = "username"
        val PREF_KEY_PASSWORD: String = "password"
        val PREF_KEY_USERRIGHT: String = "userright"
        val PREF_KEY_STAMPVALUE: String = "stampvalue"
        val PREF_KEY_GMAIL_PASSWORD: String = "gmailpwd"
    }
    // Directory name to store captured images

}