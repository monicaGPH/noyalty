package com.gph.scanner.fragment

import android.content.Intent
import android.os.Bundle
import android.view.*

import android.widget.Button
import android.widget.TextView
import com.gph.scanner.R
import com.gph.scanner.activity.NFCScannerActivity
import com.gph.scanner.activity.QRScnnerActivity
import com.gph.scanner.utils.AppConstant


/**
 * Created by asus on 07-Aug-18.
 */
class ScanFragment : SFragment() {

    private var mNfcReadFragment: NFCScannerDialogFragment? = null
    private var appInfoDialogfragment: AppInfoDialogFragment? = null

    companion object {
        lateinit var txt_logged_user: TextView
        lateinit var btn_nfc: Button
        lateinit var btn_card: Button
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View = inflater.inflate(R.layout.activity_main, container, false)
        txt_logged_user = view.findViewById<View>(R.id.txt_logged_user) as TextView
        btn_card = view.findViewById<View>(R.id.btn_card) as Button
        btn_nfc = view.findViewById<View>(R.id.btn_nfc) as Button


        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_logged_user.text = sActivity.getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERNAME, "")

        btn_card.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val intent = Intent(activity, QRScnnerActivity::class.java)
                startActivity(intent)
                activity!!.finish()

            }
        })
        btn_nfc.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val intent = Intent(activity, NFCScannerActivity::class.java)
                startActivity(intent)
                activity!!.finish()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val inflater = inflater
        if (inflater != null) {
            inflater.inflate(R.menu.menu_show_user, menu)
        }
        val shareItem = menu!!.findItem(R.id.action_edit_profile)
        shareItem.isVisible = false

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_app_info -> {
                gotoAppInfoScreen()
                return true
            }

        }

        return true
    }

    fun gotoAppInfoScreen() {

        appInfoDialogfragment = fragmentManager!!.findFragmentByTag(AppInfoDialogFragment.TAG) as AppInfoDialogFragment?
        if (appInfoDialogfragment == null) {
            appInfoDialogfragment = AppInfoDialogFragment.newInstance()
        }
        appInfoDialogfragment!!.show(fragmentManager, AppInfoDialogFragment.TAG)

    }
}