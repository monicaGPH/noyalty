package com.gph.scanner.fragment


import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.gph.scanner.R
import com.gph.scanner.activity.DrawerActivity
import com.gph.scanner.adapter.UserLogAdapter
import com.gph.scanner.model.Guest

import com.gph.scanner.model.GuestTransaction
import com.gph.scanner.model.GuestTransactionmodel
import com.gph.scanner.model.Voucher
import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.activity_logs.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger

/**
 * Created by asus on 16-Aug-18.
 */
class ViewLogsfragment : SFragment() {
    lateinit var dataBaseHandler: DatabaseHandler
    var guestTransactionModels = ArrayList<GuestTransactionmodel>()
    lateinit var adapter: UserLogAdapter
    lateinit var voucher: Voucher
    var voucher_value: String = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBaseHandler = DatabaseHandler(activity!!.applicationContext)
        return inflater.inflate(R.layout.activity_logs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //adding a layoutmanager
        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)

        adapter = UserLogAdapter(guestTransactionModels, activity!!.applicationContext, sActivity.currentLanguage)
        val guests: ArrayList<Guest> = dataBaseHandler.getGuest()!!
        if (guests.size > 0) {
            guests.forEach {
                println(it)
                var redeem = 0
                var stamp = 0
                var reward = 0
                val guest_name = it.guest_name
                val guest_add = it.guest_add
                val guest_mobile = it.guest_mobile_number
                val guest_email = it.guest_email
                com.orhanobut.logger.Logger.d(it)
                val guestTranscations: ArrayList<GuestTransaction> = dataBaseHandler.getGuestTransaction(it.guest_id.toInt())!!
                if (dataBaseHandler.checkGuestTransactionInformationExistorNot(it.guest_id)) {
                    val guestTransactionmodelOuter = dataBaseHandler.getGuestTransactionBYGuestID(it.guest_id.toInt())!!
                    redeem = guestTransactionmodelOuter.KEY_GUEST_TRANSACTION_REDEEM.toInt()
                    reward = guestTransactionmodelOuter.KEY_GUEST_TRANSACTION_REWARD
                    stamp = guestTransactionmodelOuter.KEY_GUEST_TRANSACTION_STAMP
                }
                if (dataBaseHandler.checkVoucherInformationExistorNot(it.guest_id)) {
                    voucher = dataBaseHandler.getVoucherInformation(it.guest_id)!!
                    voucher_value = voucher.voucher_value
                }
                guestTranscations.forEach {

                    println(it)
                    val guestTransactionModel = GuestTransactionmodel(
                            guest_name,
                            guest_add,
                            guest_mobile,
                            guest_email,
                            it.KEY_GUEST_TRANSACTION_ACTIVITY_ID,
                            it.KEY_GUEST_TRANSACTION_ID,
                            it.KEY_GUEST_TRANSACTION_REDEEM,
                            it.KEY_GUEST_TRANSACTION_REWARD,
                            it.KEY_GUEST_TRANSACTION_STAMP,
                            it.KEY_GUEST_TRANSACTION_UPDATEDBY,
                            it.KEY_GUEST_TRANSACTION_UPDATED,
                            redeem,
                            reward,
                            stamp,
                            if (voucher_value.isNullOrEmpty()) 0.0f else voucher.voucher_value.toFloat())

                    guestTransactionModels.add(guestTransactionModel)
                    val sdf = SimpleDateFormat("dd/M/yyyy  HH:mm")
                    // public fun <T> Array<out T>.sortWith(comparator: Comparator<in T>): Unit
                    // -> Sorts the array in-place according to the order specified by the given [comparator].
                    guestTransactionModels.sortWith(object : Comparator<GuestTransactionmodel> {
                        override fun compare(object1: GuestTransactionmodel, object2: GuestTransactionmodel): Int = when {
                            sdf.parse(object2.KEY_GUEST_TRANSACTION_UPDATED) > sdf.parse(object1.KEY_GUEST_TRANSACTION_UPDATED) -> 1
                            sdf.parse(object2.KEY_GUEST_TRANSACTION_UPDATED) == sdf.parse(object1.KEY_GUEST_TRANSACTION_UPDATED) -> 0
                            else -> -1
                        }
                    })

                    adapter.notifyDataSetChanged()

                }
            }
            //creating our adapter


            //now adding the adapter to recyclerview
            recyclerView.adapter = adapter


        } else {

            showMessageOK(getString(R.string.customer_empty_list_alert_text_string))
        }

        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(arg0: Editable) {
                // TODO Auto-generated method stub
                filter(arg0.toString())
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int,
                                           arg2: Int, arg3: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int,
                                       arg3: Int) {
                // TODO Auto-generated method stub


            }
        })
    }

    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdNames = ArrayList<GuestTransactionmodel>()

        //looping through existing elements
        for (guestTransactionmodel in guestTransactionModels) {
            //if the existing elements contains the search input

            if (guestTransactionmodel.KEY_GUEST_TRANSACTION_UPDATEDBY.toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(guestTransactionmodel)
            }

        }

        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames)
    }

    fun callActivity() {
        val intent = Intent(activity, DrawerActivity::class.java)
        startActivity(intent)
        activity!!.finish()
    }


    private fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(activity)

        // Set the alert dialog title
        builder.setTitle("Confirmation")

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            callActivity()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }


}
