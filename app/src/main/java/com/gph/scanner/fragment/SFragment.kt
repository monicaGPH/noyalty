package com.gph.scanner.fragment

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.gph.scanner.R
import com.gph.scanner.activity.SActivity
import com.gph.scanner.utils.ForegroundTaskDelegate
import java.util.*

/**
 * Created by asus on 07-Aug-18.
 */
open class SFragment : Fragment() {

    @LayoutRes
    protected fun getRootLayoutRes(): Int {
        return R.layout.holder_empty
    }

    protected lateinit var listOfForegroundTaskDelegates: List<ForegroundTaskDelegate<Any>>
    lateinit var sActivity: SActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is SActivity) {
            sActivity = context as SActivity
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(getRootLayoutRes(), container, false)
        ButterKnife.bind(this, rootView)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listOfForegroundTaskDelegates = Vector<ForegroundTaskDelegate<Any>>()
    }

    override fun onDestroyView() {
        for (delegate in listOfForegroundTaskDelegates) {
            if (delegate != null) {
                delegate!!.cancel()
            }
        }
        super.onDestroyView()
    }
}