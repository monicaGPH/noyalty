package com.gph.scanner.fragment.dialog

import android.support.v4.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.gph.scanner.R
import com.gph.scanner.activity.SActivity
import com.gph.scanner.utils.ForegroundTaskDelegate
import java.util.*

/**
 * Created by asus on 13-Sep-18.
 */
open class SDialogFragment :DialogFragment(){

@LayoutRes
fun getRootLayoutRes(): Int {
    return R.layout.holder_empty
}

protected lateinit var listOfForegroundTaskDelegates: List<ForegroundTaskDelegate<Any>>
lateinit var sActivity: SActivity

override fun onAttach(context: Context?) {
    super.onAttach(context)

    if (context is SActivity) {
        sActivity = context as SActivity
    }
}

override fun onCreateView(inflater: LayoutInflater,
                          container: ViewGroup?,
                          savedInstanceState: Bundle?): View? {

    val rootView = inflater.inflate(getRootLayoutRes(), container, false)
    ButterKnife.bind(this, rootView)

    return rootView
}

override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    listOfForegroundTaskDelegates = Vector<ForegroundTaskDelegate<Any>>()
}

override fun onDestroyView() {
    for (delegate in listOfForegroundTaskDelegates) {
        if (delegate != null) {
            delegate!!.cancel()
        }
    }
    super.onDestroyView()
}
}