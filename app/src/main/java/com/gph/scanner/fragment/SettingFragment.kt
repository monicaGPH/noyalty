package com.gph.scanner.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gph.scanner.R
import com.gph.scanner.activity.ViewCustomerActivity
import com.gph.scanner.activity.ViewUserActivity
import com.gph.scanner.fragment.dialog.StampDialog
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.layout_setting.*

/**
 * Created by asus on 13-Sep-18.
 */
class SettingFragment : SFragment() {

    lateinit var dataBaseHandler: DatabaseHandler
    lateinit var role: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       // crete database's class instance
        dataBaseHandler = DatabaseHandler(activity!!.applicationContext)
        // get user right of logged in user
        role = sActivity.getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERRIGHT, "")
        return inflater.inflate(R.layout.layout_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    // set visiblity of layout according to the role of logged in User
        if (role == "admin") {
            // role = 'admin'
            //set visiblity of  layout to VISIBLE
            layout_stamp_value.visibility = View.VISIBLE
            //set visiblity of  layout to VISIBLE
            layout_user_list.visibility = View.VISIBLE
          //  set visiblity of  layout to VISIBLE
            layout_view_customer.visibility = View.VISIBLE
        } else {
            // role = 'user'
            // set visiblity of  layout to GONE
            layout_stamp_value.visibility = View.GONE
            //set visiblity of  layout to GONE
            layout_user_list.visibility = View.GONE
           // set visiblity of  layout to VISIBLE
            layout_view_customer.visibility = View.VISIBLE
        }
        // onClick events on layout
        layout_stamp_value.setOnClickListener {
            val stampDialog = StampDialog()
            stampDialog.show(fragmentManager, "")

        }
        // onClick events on layout
        layout_view_customer.setOnClickListener {
            val intent = Intent(activity, ViewCustomerActivity::class.java)
            intent.putExtra("INTENT_EXTRA_CALL_FROM_ACTIVITY", "SETTING")
            startActivity(intent)
        }
        // onClick events on layout
        layout_user_list.setOnClickListener {
            val intent = Intent(activity, ViewUserActivity::class.java)
            startActivity(intent)
        }

    }
}