package com.gph.scanner.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.activity.DrawerActivity
import com.gph.scanner.model.Store
import com.gph.scanner.utils.GMailSender
import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.activity_add_store.*




/**
 * Created by asus on 16-Aug-18.
 */
class AddStoreFragment : SFragment() {
    lateinit var dataBaseHandler: DatabaseHandler
    var hasValue: Boolean = false
    lateinit var store: Store


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBaseHandler = DatabaseHandler(activity!!.applicationContext)
        return inflater.inflate(R.layout.activity_add_store, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (dataBaseHandler.checkStoreExistorNot()) {
            store = dataBaseHandler.getStore()!!
            hasValue = true
            et_store_name.setText(store.store_name)
            et_store_address.setText(store.store_address)
            et_store_email_address.setText(store.store_email_address)
            add_store_title.text = getString(R.string.update_store_info_text_string)
            btn_add.text = getString(R.string.update_string)

        } else {
            add_store_title.text = getString(R.string.add_store_info_text_string)
            btn_add.text = getString(R.string.add_string)
        }



        btn_add.setOnClickListener {
            if (!TextUtils.isEmpty(et_store_name.text.toString().trim())
                    && !TextUtils.isEmpty(et_store_address.text.toString().trim())
                    && !TextUtils.isEmpty(et_store_email_address.text.toString().trim())) {
                if (sActivity.isEmailValid(et_store_email_address.text.toString().trim())) {
                    if (hasValue) {

                        showMessageOK(getString(R.string.update_store_info_alert_text_string))
                    } else {
                        dataBaseHandler.addStore(
                                Store(et_store_name.text.toString().trim(),
                                        et_store_address.text.toString().trim(),
                                        et_store_email_address.text.toString().trim(),
                                        ""))
                        et_store_address.text.clear()
                        et_store_name.text.clear()
                        Toast.makeText(activity, getString(R.string.store_info_insert_toast_text_string), Toast.LENGTH_SHORT).show()
                        callActivity()
                    }
                } else {
                    et_store_email_address.error = getString(R.string.email_format_error_hint_string)
                    et_store_email_address.requestFocus()
                }

            } else {
                if (TextUtils.isEmpty(et_store_name.text.toString().trim())) {
                    et_store_name.error = getString(R.string.store_name_empty_text_string)
                    et_store_name.requestFocus()
                } else if (TextUtils.isEmpty(et_store_address.text.toString().trim())) {
                    et_store_address.error = getString(R.string.store_address_text_string)
                    et_store_address.requestFocus()
                }
            }
        }
    }


    private fun showMessageOK(message: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(activity)

        // Set the alert dialog title
        builder.setTitle("Confirmation")

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            dataBaseHandler.updateStore(Store(
                    et_store_name.text.toString().trim(),
                    et_store_address.text.toString().trim(),
                    et_store_email_address.text.toString().trim(),
                    ""), store.store_id)
            et_store_address.text.clear()
            et_store_name.text.clear()
            Toast.makeText(activity, getString(R.string.store_info_update_toast_text_string), Toast.LENGTH_SHORT).show()
            callActivity()
        }
        // Set a negative button and its click listener on alert dialog
        builder.setNegativeButton("CANCEL") { dialog, _ ->
            dialog.dismiss()

        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }

    private fun callActivity() {
        val intent = Intent(activity, DrawerActivity::class.java)
        startActivity(intent)
        activity!!.finish()
    }
}

