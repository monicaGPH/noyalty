package com.gph.scanner.fragment


import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gph.scanner.R


/**
 * Created by asus on 18-Aug-18.
 */
class AppInfoDialogFragment : DialogFragment() {

    companion object {
        var TAG: String = AppInfoDialogFragment::javaClass.name
        fun newInstance(): AppInfoDialogFragment {
            return AppInfoDialogFragment()
        }
    }

    private lateinit var mTvMessage: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.layout_app_info, container, false)
        initViews(view)
        return view

    }

    private fun initViews(view: View) {

        mTvMessage = view.findViewById(R.id.tv_message)

        /*
        * get version code of noyalty app
        */
        val manager = context!!.packageManager
        var info: PackageInfo? = null
        try {
            info = manager.getPackageInfo(
                    context!!.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        mTvMessage.text = info!!.versionName

    }


}