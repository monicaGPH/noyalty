package com.gph.scanner.fragment.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.utils.AppConstant

/**
 * Created by asus on 13-Sep-18.
 */
class StampDialog : SDialogFragment() {
    lateinit var et_stamp_value: EditText
    lateinit var btn_yes: Button
    lateinit var btn_no: Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val stampValue = sActivity.getScannerApplication().getAppPreferences()!!.getInt(AppConstant.PREF_KEY_STAMPVALUE, 0)
        if (stampValue == 0)
            et_stamp_value.setText("" + 5)
        else
            et_stamp_value.setText("" + stampValue)

        btn_no.setOnClickListener {
            dismiss()
        }
        btn_yes.setOnClickListener {
            sActivity.getScannerApplication().getAppPreferences()!!.edit().putInt(AppConstant.PREF_KEY_STAMPVALUE, et_stamp_value.text.toString().toInt()).apply()
            Toast.makeText(context, getString(R.string.stamp_toast_text_string), Toast.LENGTH_SHORT).show()
            dismiss()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.custom_dialog, container, false)
        //  disable onClick outside
        dialog.setCanceledOnTouchOutside(false)
        // move layout on keyboard open
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        et_stamp_value = view.findViewById(R.id.et_stamp_value)
        btn_no = view.findViewById(R.id.btn_no)
        btn_yes = view.findViewById(R.id.btn_yes)
        return view
    }

}