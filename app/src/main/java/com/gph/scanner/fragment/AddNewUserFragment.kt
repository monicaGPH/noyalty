package com.gph.scanner.fragment


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.gph.scanner.R
import com.gph.scanner.activity.DrawerActivity
import com.gph.scanner.model.Account

import com.gph.scanner.utils.database.DatabaseHandler
import kotlinx.android.synthetic.main.activity_new_user.*

import java.util.*


/**
 * Created by asus on 05-Sep-18.
 */
class AddNewUserFragment : SFragment() {
    lateinit var dataBaseHandler: DatabaseHandler
    lateinit var role: String
    lateinit var existingLocale:Locale


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBaseHandler = DatabaseHandler(activity!!.applicationContext)
        return inflater.inflate(R.layout.activity_new_user, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        existingLocale = sActivity.currentLanguage
        btn_addUser.setOnClickListener {
            if (checkBox_isAdmin.isChecked) {
                role = "admin"
            } else {
                role = "normal"
            }
            if (!TextUtils.isEmpty(et_name.text.toString().trim()) &&
                    !TextUtils.isEmpty(et_email.text.toString().trim()) &&
                    !TextUtils.isEmpty(et_password.text.toString().trim())) {
                if (sActivity.isEmailValid(et_email.text.toString().trim())) {
                    dataBaseHandler.addEmployee(Account(
                            et_name.text.toString().trim(),
                            et_email.text.toString().trim(),
                            et_password.text.toString().trim(),
                            role))

                    et_name.text.clear()
                    et_email.text.clear()
                    et_password.text.clear()
                    if(existingLocale.toLanguageTag() == "en")
                    Toast.makeText(activity, getString(R.string.user_information_toast_string), Toast.LENGTH_SHORT).show()
                    else
                        Toast.makeText(activity, getString(R.string.user_information_toast_string), Toast.LENGTH_SHORT).show()
                    callActivity()
                } else {
                    if(existingLocale.toLanguageTag() =="en")
                    et_email.error = getString(R.string.email_format_error_hint_string)
                    else{
                        et_email.error = getString(R.string.email_format_error_hint_string)
                    }
                    et_email.requestFocus()

                }

            } else {
                when {
                    TextUtils.isEmpty(et_name.text.toString().trim()) -> {
                       if(existingLocale.toLanguageTag() == "en")
                           et_name.error = getString(R.string.name_error_hint_string)
                        else
                           et_name.error = getString(R.string.name_error_hint_string)
                        et_name.requestFocus()
                    }
                    TextUtils.isEmpty(et_email.text.toString().trim()) -> {
                      if(existingLocale.toLanguageTag() == "en")
                          et_email.error = getString(R.string.email_address_empty_error_string)
                        else
                          et_email.error = getString(R.string.email_address_empty_error_string)
                        et_email.requestFocus()
                    }
                    TextUtils.isEmpty(et_password.text.toString().trim()) -> {
                        if(existingLocale.toLanguageTag() == "en")
                        et_password.error = getString(R.string.pwd_empty_error_String)
                        else
                            et_password.error = getString(R.string.pwd_empty_error_String)
                        et_password.requestFocus()
                    }
                }
            }
        }
    }


    private fun callActivity() {
        val intent = Intent(activity, DrawerActivity::class.java)
        startActivity(intent)
        activity!!.finish()
    }


}