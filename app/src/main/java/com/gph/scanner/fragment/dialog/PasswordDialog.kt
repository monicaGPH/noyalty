package com.gph.scanner.fragment.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.activity.DashBoardActivity
import com.gph.scanner.utils.AppConstant

/**
 * Created by asus on 17-Sep-18.
 */
class PasswordDialog : SDialogFragment() {
    lateinit var et_pwd_value: EditText
    lateinit var btn_yes: Button
    lateinit var btn_no: Button


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // request a window without the title
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_no.setOnClickListener {
            dismiss()
          //  DashBoardActivity().showMessageOK(getString(R.string.send_email_alert_text_string), true, "sendEmail")

        }
        btn_yes.setOnClickListener {
            if (!et_pwd_value.text.isNullOrEmpty()) {
                sActivity.getScannerApplication().getAppPreferences()!!.edit().putString(AppConstant.PREF_KEY_GMAIL_PASSWORD, et_pwd_value.text.toString()).apply()
            } else {
                Toast.makeText(context, "Please enter gmail password first", Toast.LENGTH_SHORT).show()
            }
            dismiss()
            //DashBoardActivity().showMessageOK(getString(R.string.send_email_alert_text_string), true, "sendEmail")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.password_dialog, container, false)
        //  disable onClick outside
        dialog.setCanceledOnTouchOutside(false)
        // move layout on keyboard open
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        et_pwd_value = view.findViewById(R.id.et_pwd_value)
        btn_no = view.findViewById(R.id.btn_no)
        btn_yes = view.findViewById(R.id.btn_yes)
        return view
    }

}