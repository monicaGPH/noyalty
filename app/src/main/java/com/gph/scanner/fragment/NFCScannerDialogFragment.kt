package com.gph.scanner.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.gph.scanner.R
import com.gph.scanner.activity.AddNewCustomerActivity
import com.gph.scanner.activity.DashBoardActivity
import com.gph.scanner.activity.NFCScannerActivity
import com.gph.scanner.fragment.dialog.SDialogFragment
import com.gph.scanner.model.Guest
import com.gph.scanner.utils.AppConstant
import com.gph.scanner.utils.Listener
import com.gph.scanner.utils.database.DatabaseHandler
import com.orhanobut.logger.Logger
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by asus on 07-Aug-18.
 */
class NFCScannerDialogFragment : SDialogFragment() {

    companion object {
        var TAG: String = NFCScannerDialogFragment::javaClass.name
        fun newInstance(): NFCScannerDialogFragment {
            return NFCScannerDialogFragment()
        }
    }

    private lateinit var mTvMessage: TextView
    private lateinit var mListener: Listener
    private var dataBaseHandler: DatabaseHandler? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.layout_nfc_read, container, false)
        initViews(view)
        return view
    }

    private fun initViews(view: View) {
        mTvMessage = view.findViewById(R.id.tv_message)
        dataBaseHandler = DatabaseHandler(this.activity!!)
    }

    @Suppress("DEPRECATION")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        mListener = context as NFCScannerActivity
        mListener.onDialogDisplayed()
    }

    override fun onDetach() {
        super.onDetach()
        mListener.onDialogDismissed()
    }

    fun onNfcDetected(ndef: ByteArray) {
        readFromNFC(ndef)
    }

    @SuppressLint("SimpleDateFormat")
    private fun readFromNFC(ndef: ByteArray) {

        try {
            val message: String = ByteArrayToHexString(ndef)
            Logger.i(TAG, "readFromNFC: " + message)
            mTvMessage.text = message

            val sdf = SimpleDateFormat("dd/M/yyyy HH:mm")
            val currentDate = sdf.format(Date())

            if (!dataBaseHandler!!.checkGuestEmployeeExistorNotNFC(message)) {

                //  if (sActivity.getScannerApplication().getAppPreferences()!!.getString(AppConstant.PREF_KEY_USERRIGHT, "") == "admin") {
                dataBaseHandler!!.addGuest(com.gph.scanner.model.Guest(
                        "",
                        "",
                        "",
                        "",
                        "",
                        message,
                        "",
                        "",
                        currentDate))


                /*      val intent = Intent(activity, AddNewCustomerActivity::class.java)
                      intent.putExtra("INTENT_EXTRA_SCAN_TYPE", "NFC")
                      intent.putExtra("INTENT_EXTRA_SCAN_ID", message)
                      intent.putExtra("INTENT_EXTRA_CALL_FROM_WHICH_ACTIVITY", "ScannerClass")
                      startActivity(intent)
                      activity!!.finish()*/

            }
            val guest: Guest? = dataBaseHandler!!.getGuestBYnfcId(message)
            val intent = Intent(activity, DashBoardActivity::class.java)
            if (guest != null) {
                intent.putExtra("INTENT_EXTRA_SCAN_TYPE", "NFC")
                intent.putExtra("INTENT_EXTRA_SCAN_ID", message)
            }
            startActivity(intent)
            activity!!.finish()
            /*else {

              *//*  val guest: Guest? = dataBaseHandler!!.getGuestBYnfcId(message)
                val intent = Intent(activity, DashBoardActivity::class.java)
                if (guest != null) {

                    intent.putExtra("INTENT_EXTRA_SCAN_TYPE", "NFC")
                    intent.putExtra("INTENT_EXTRA_SCAN_ID", message)

                }
                startActivity(intent)
                activity!!.finish()*//*
            }*/


        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }


    /* private fun readFromNFC(ndef: Ndef) {

         try {
             ndef.connect()


             var ndefMessage: NdefMessage = ndef.ndefMessage
             var message: String = String(ndefMessage.records[0].payload)
             Logger.i(TAG, "readFromNFC: " + message)
             mTvMessage.text = message
             if (!dataBaseHandler!!.checkGuestEmployeeExistorNotNFC(message)) {
                 Toast.makeText(activity, "new user", Toast.LENGTH_SHORT).show()
                 dataBaseHandler!!.addGuest(com.gph.scanner.model.Guest("", "", "", "", "", "", message))

             } else {
                 Toast.makeText(activity, "old user", Toast.LENGTH_SHORT).show()
                 val guest: Guest? = dataBaseHandler!!.getGuestBYnfcId(message)
             }
             ndef.close()

         } catch (e: IOException) {
             e.printStackTrace()

         }
     }*/

    public fun ByteArrayToHexString(inarray: ByteArray): String {
        var i: Int
        var j = 0
        var `in`: Int
        val hex = arrayOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")
        var out = ""

        while (j < inarray.size) {
            `in` = inarray[j].toInt() and 0xff
            i = `in` shr 4 and 0x0f
            out += hex[i]
            i = `in` and 0x0f
            out += hex[i]
            ++j
        }
        return out

    }

}